﻿namespace WorkFlow.Services.Models
{
    public class AssignmentRule : EngineEntity
    {
        public string Name { get; set; }
        public string AssignedTo { get; set; }
    }
}
