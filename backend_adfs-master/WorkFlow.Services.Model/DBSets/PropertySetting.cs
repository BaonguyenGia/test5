﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class PropertySetting : EngineEntity
    {
        public long PropertyId { get; set; }
        public bool IsContent { get; set; }
        public bool IsHtml { get; set; }
        public int? Row { get; set; }
        public int? Column { get; set; }
        #region Choice
        public ChoiceDisplay Display { get; set; }
        #endregion
        #region Number & Currency
        public decimal? Minimum { get; set; }
        public decimal? Maximum { get; set; }
        public int NumberOfDecimalPlaces { get; set; }
        public bool IsPercentages { get; set; }
        public bool CurrencyFormat { get; set; }
        #endregion
        #region Date & Time
        public bool IsDateOnly { get; set; }
        #endregion
        #region User
        public bool AllowMultiple { get; set; }
        #endregion
        #region Calculated
        public string Formula { get; set; }
        #endregion
        #region Foreign
        [ForeignKey("SettingId")]
        public ICollection<PropertyChoice> Choices { get; set; }
        #endregion
    }
}
