﻿using System.Collections.Generic;

namespace WorkFlow.Services.Models
{
    public class MailModule : EngineEntity
    {
        public string Name { get; set; }

        #region Foreign
        public ICollection<MailTemplate> MailTemplates { get; set; }
        #endregion
    }
}
