﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class Department : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
        [ForeignKey("Parent")]
        public long? ParentId { get; set; }
        public string ChartCode { get; set; }
        [ForeignKey("Manager")]
        public long? ManagerId { get; set; }
        public int DeptLevel { get; set; }
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string SiteName { get; set; }
        public int? Order { get; set; }
        #region Foreign
        public Department Parent { get; set; }
        public PersonalProfile Manager { get; set; }
        [ForeignKey("ParentId")]
        public ICollection<Department> ChildDepartments { get; set; }
        #endregion

    }
}
