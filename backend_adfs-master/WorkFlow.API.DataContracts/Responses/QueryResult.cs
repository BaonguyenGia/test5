﻿using System.Collections.Generic;

namespace WorkFlow.API.DataContracts
{
    public class QueryResult<T>
    {
        public long TotalItems { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
