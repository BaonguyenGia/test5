﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class PropertyDefaultValueTypeResource : EngineEntity
    {
        public string Name { get; set; }
    }
}
