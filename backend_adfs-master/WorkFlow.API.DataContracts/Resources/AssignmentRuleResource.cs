﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class AssignmentRuleResource : EngineEntity
    {
        public string Name { get; set; }
        public string AssignedTo { get; set; }
    }
}
