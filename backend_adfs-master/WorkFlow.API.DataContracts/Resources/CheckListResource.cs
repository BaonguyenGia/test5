﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class CheckListResource : EngineEntity
    {
        public string Title { get; set; }
        public long? WorkflowStepId { get; set; }
        public long? TypeId { get; set; }
        public bool IsRequired { get; set; }
        public bool IsQRCode { get; set; }
        public bool IsSignature { get; set; }
        #region  Foreign
        public DocumentTypeResource DocumentType { get; set; }
        #endregion
    }
}
