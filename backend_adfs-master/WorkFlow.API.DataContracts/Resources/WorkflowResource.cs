﻿using System.Collections.Generic;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class WorkflowResource : EngineEntity
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public WorkflowStatus Status { get; set; }
        public string Version { get; set; }
        public string CreatePermission { get; set; }
        public string SeenPermission { get; set; }
        public string ImageURL { get; set; }
        public bool IsAttachmentActive { get; set; }
        public string AllowAttachmentExtensions { get; set; }
        public int MaximumSizeSingle { get; set; }
        public int MaximumSizeTotal { get; set; }
        public long? CategoryId { get; set; }
        public int? Order { get; set; }
        public WorkflowCategoryResource Category { get; set; }
        public ICollection<PropertyResource> Properties { get; set; }
    }
}
