namespace WorkFlow.Tools.HttpContext
{
    public interface IHttpContextHelper
    {
        string GetCurrentUser();
    }
}
