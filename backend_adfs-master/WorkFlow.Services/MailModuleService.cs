﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class MailModuleService : IMailModuleService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<MailModuleService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public MailModuleService(IMapper mapper, ILogger<MailModuleService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<MailModule>> CreateMailModule(MailModuleResource mailModuleResource)
        {
            const string loggerHeader = "CreateMailModule";

            var apiResponse = new ApiResponse<MailModule>();
            MailModule mailModule = _mapper.Map<MailModuleResource, MailModule>(mailModuleResource);

            _logger.LogDebug($"{loggerHeader} - Start to add MailModule: {JsonConvert.SerializeObject(mailModule)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    mailModule.CreatedBy = _httpContextHelper.GetCurrentUser();
                    mailModule.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.MailModuleRepository.Add(mailModule);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new MailModule successfully with Id: {mailModule.Id}");
                    apiResponse.Data = await unitOfWork.MailModuleRepository.FindFirst(d => d.Id == mailModule.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<MailModule>> UpdateMailModule(long id, MailModuleResource mailModuleResource)
        {
            const string loggerHeader = "UpdateMailModule";
            var apiResponse = new ApiResponse<MailModule>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var mailModule = await unitOfWork.MailModuleRepository.FindFirst(d => d.Id == id);
                    mailModule = _mapper.Map<MailModuleResource, MailModule>(mailModuleResource, mailModule);
                    _logger.LogDebug($"{loggerHeader} - Start to update MailModule: {JsonConvert.SerializeObject(mailModule)}");

                    mailModule.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    mailModule.LastModified = DateTime.UtcNow;
                    unitOfWork.MailModuleRepository.Update(mailModule);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update MailModule successfully with Id: {mailModule.Id}");

                    apiResponse.Data = await unitOfWork.MailModuleRepository.FindFirst(d => d.Id == mailModule.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<MailModule>> DeleteMailModule(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteMailModule";

            var apiResponse = new ApiResponse<MailModule>();

            _logger.LogDebug($"{loggerHeader} - Start to delete MailModule with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var mailModule = await unitOfWork.MailModuleRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.MailModuleRepository.Remove(mailModule);
                    }
                    else
                    {

                        mailModule.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        mailModule.IsDeleted = true;
                        mailModule.LastModified = DateTime.UtcNow;
                        unitOfWork.MailModuleRepository.Update(mailModule);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete MailModule successfully with Id: {mailModule.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<MailModule>> GetMailModule(long id)
        {
            const string loggerHeader = "UpdateMailModule";

            var apiResponse = new ApiResponse<MailModule>();

            _logger.LogDebug($"{loggerHeader} - Start to get MailModule with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    apiResponse.Data = await unitOfWork.MailModuleRepository.FindFirst(d => d.Id == id);
                    _logger.LogDebug($"{loggerHeader} - Get MailModule successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<MailModule>>> GetMailModules(QueryResource queryObj)
        {
            const string loggerHeader = "GetMailModules";

            var apiResponse = new ApiResponse<QueryResult<MailModule>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get MailModules with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<MailModule, object>>>()
                    {
                        ["name"] = s => s.Name
                    };

                    var query = await unitOfWork.MailModuleRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Name, $"%{queryObj.Name}%")),
                                                                        include: source => source.Include(d => d.MailTemplates),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - Get MailModules successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
