﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class WorkweekService : IWorkweekService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<WorkweekService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public WorkweekService(IMapper mapper, ILogger<WorkweekService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<Workweek>> CreateWorkweek(WorkweekResource workweekResource)
        {
            const string loggerHeader = "CreateWorkweek";

            var apiResponse = new ApiResponse<Workweek>();
            Workweek workweek = _mapper.Map<WorkweekResource, Workweek>(workweekResource);

            _logger.LogDebug($"{loggerHeader} - Start to add Workweek: {JsonConvert.SerializeObject(workweek)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    workweek.CreatedBy = _httpContextHelper.GetCurrentUser();
                    workweek.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.WorkweekRepository.Add(workweek);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new Workweek successfully with Id: {workweek.Id}");
                    apiResponse.Data = await unitOfWork.WorkweekRepository.FindFirst(d => d.Id == workweek.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Workweek>> UpdateWorkweek(long id, WorkweekResource workweekResource)
        {
            const string loggerHeader = "UpdateWorkweek";
            var apiResponse = new ApiResponse<Workweek>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workweek = await unitOfWork.WorkweekRepository.FindFirst(d => d.Id == id);
                    workweek = _mapper.Map<WorkweekResource, Workweek>(workweekResource, workweek);
                    _logger.LogDebug($"{loggerHeader} - Start to update Workweek: {JsonConvert.SerializeObject(workweek)}");

                    workweek.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workweek.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkweekRepository.Update(workweek);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update Workweek successfully with Id: {workweek.Id}");

                    apiResponse.Data = await unitOfWork.WorkweekRepository.FindFirst(d => d.Id == workweek.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Workweek>> DeleteWorkweek(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteWorkweek";

            var apiResponse = new ApiResponse<Workweek>();

            _logger.LogDebug($"{loggerHeader} - Start to delete Workweek with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workweek = await unitOfWork.WorkweekRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.WorkweekRepository.Remove(workweek);
                    }
                    else
                    {
                        workweek.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workweek.IsDeleted = true;
                        workweek.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkweekRepository.Update(workweek);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete Workweek successfully with Id: {workweek.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Workweek>> GetWorkweek(long id)
        {
            const string loggerHeader = "UpdateWorkweek";

            var apiResponse = new ApiResponse<Workweek>();

            _logger.LogDebug($"{loggerHeader} - Start to get Workweek with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    apiResponse.Data = await unitOfWork.WorkweekRepository.FindFirst(d => d.Id == id);
                    _logger.LogDebug($"{loggerHeader} - Get Workweek successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<Workweek>>> GetWorkweeks(QueryResource queryObj)
        {
            const string loggerHeader = "GetWorkweeks";

            var apiResponse = new ApiResponse<QueryResult<Workweek>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get Workweeks with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Workweek, object>>>()
                    {
                        ["title"] = s => s.Title
                    };

                    var query = await unitOfWork.WorkweekRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: null,
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - Get Workweeks successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
