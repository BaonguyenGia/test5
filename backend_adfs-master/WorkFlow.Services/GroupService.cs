﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class GroupService : IGroupService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<GroupService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public GroupService(IMapper mapper, ILogger<GroupService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {
            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<Group>> CreateGroup(GroupResource groupResource)
        {
            const string loggerHeader = "CreateGroup";

            var apiResponse = new ApiResponse<Group>();
            Group group = _mapper.Map<GroupResource, Group>(groupResource);

            _logger.LogDebug($"{loggerHeader} - Start to add Group: {JsonConvert.SerializeObject(group)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    group.CreatedBy = _httpContextHelper.GetCurrentUser();
                    group.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.GroupRepository.Add(group);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new Group successfully with Id: {group.Id}");
                    apiResponse.Data = await unitOfWork.GroupRepository.FindFirst(d => d.Id == group.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Group>> UpdateGroup(long id, GroupResource groupResource)
        {
            const string loggerHeader = "UpdateGroup";
            var apiResponse = new ApiResponse<Group>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var group = await unitOfWork.GroupRepository.FindFirst(d => d.Id == id);
                    group = _mapper.Map<GroupResource, Group>(groupResource, group);
                    _logger.LogDebug($"{loggerHeader} - Start to update Group: {JsonConvert.SerializeObject(group)}");

                    group.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    group.LastModified = DateTime.UtcNow;
                    unitOfWork.GroupRepository.Update(group);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update Group successfully with Id: {group.Id}");

                    apiResponse.Data = await unitOfWork.GroupRepository.FindFirst(d => d.Id == group.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Group>> DeleteGroup(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteGroup";

            var apiResponse = new ApiResponse<Group>();

            _logger.LogDebug($"{loggerHeader} - Start to delete Group with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var group = await unitOfWork.GroupRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.GroupRepository.Remove(group);
                    }
                    else
                    {
                        group.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        group.IsDeleted = true;
                        group.LastModified = DateTime.UtcNow;
                        unitOfWork.GroupRepository.Update(group);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete Group successfully with Id: {group.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Group>> GetGroup(long id)
        {
            const string loggerHeader = "UpdateGroup";

            var apiResponse = new ApiResponse<Group>();

            _logger.LogDebug($"{loggerHeader} - Start to get Group with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    apiResponse.Data = await unitOfWork.GroupRepository.FindFirst(d => d.Id == id);
                    _logger.LogDebug($"{loggerHeader} - Get Group successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<Group>>> GetGroups(QueryResource queryObj)
        {
            const string loggerHeader = "GetGroups";

            var apiResponse = new ApiResponse<QueryResult<Group>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get Groups with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Group, object>>>()
                    {
                        ["title"] = s => s.Title
                    };

                    var query = await unitOfWork.GroupRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: source => source.Include(d => d.PersonalProfiles),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - Get Groups successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
