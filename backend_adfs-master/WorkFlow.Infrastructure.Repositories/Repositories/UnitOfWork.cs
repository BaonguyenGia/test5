﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Threading.Tasks;
using WorkFlow.Infrastructure.Data;
using WorkFlow.Services.Models;

namespace WorkFlow.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WorkFlowDbContext _context;
        private readonly ILogger _logger;

        #region DBSet Repotiory
        private IAsyncRepository<Department> _departmentRepository;
        private IAsyncRepository<PersonalProfile> _personalProfileRepository;
        private IAsyncRepository<Position> _positionRepository;
        private IAsyncRepository<DocumentType> _documentTypeRepository;
        private IAsyncRepository<Workweek> _workweekRepository;
        private IAsyncRepository<WorkingTime> _workingTimeRepository;
        private IAsyncRepository<WorkflowCategory> _workflowCategoryRepository;
        private IAsyncRepository<Supplier> _supplierRepository;
        private IAsyncRepository<MailTemplate> _mailTemplateRepository;
        private IAsyncRepository<MailModule> _mailModuleRepository;
        private IAsyncRepository<ErrorLog> _errorLogRepository;
        private IAsyncRepository<Property> _propertyRepository;
        private IAsyncRepository<Group> _groupRepository;
        private IAsyncRepository<Workflow> _workflowRepository;
        private IAsyncRepository<WorkflowStep> _workflowStepRepository;
        public IAsyncRepository<Department> DepartmentRepository => _departmentRepository ?? (_departmentRepository = new EfRepository<Department>(_context));
        public IAsyncRepository<PersonalProfile> PersonalProfileRepository => _personalProfileRepository ?? (_personalProfileRepository = new EfRepository<PersonalProfile>(_context));
        public IAsyncRepository<Position> PositionRepository => _positionRepository ?? (_positionRepository = new EfRepository<Position>(_context));
        public IAsyncRepository<DocumentType> DocumentTypeRepository => _documentTypeRepository ?? (_documentTypeRepository = new EfRepository<DocumentType>(_context));
        public IAsyncRepository<Workweek> WorkweekRepository => _workweekRepository ?? (_workweekRepository = new EfRepository<Workweek>(_context));
        public IAsyncRepository<WorkingTime> WorkingTimeRepository => _workingTimeRepository ?? (_workingTimeRepository = new EfRepository<WorkingTime>(_context));
        public IAsyncRepository<WorkflowCategory> WorkflowCategoryRepository => _workflowCategoryRepository ?? (_workflowCategoryRepository = new EfRepository<WorkflowCategory>(_context));
        public IAsyncRepository<Supplier> SupplierRepository => _supplierRepository ?? (_supplierRepository = new EfRepository<Supplier>(_context));
        public IAsyncRepository<MailTemplate> MailTemplateRepository => _mailTemplateRepository ?? (_mailTemplateRepository = new EfRepository<MailTemplate>(_context));
        public IAsyncRepository<MailModule> MailModuleRepository => _mailModuleRepository ?? (_mailModuleRepository = new EfRepository<MailModule>(_context));
        public IAsyncRepository<ErrorLog> ErrorLogRepository => _errorLogRepository ?? (_errorLogRepository = new EfRepository<ErrorLog>(_context));
        public IAsyncRepository<Property> PropertyRepository => _propertyRepository ?? (_propertyRepository = new EfRepository<Property>(_context));
        public IAsyncRepository<Group> GroupRepository => _groupRepository ?? (_groupRepository = new EfRepository<Group>(_context));
        public IAsyncRepository<Workflow> WorkflowRepository => _workflowRepository ?? (_workflowRepository = new EfRepository<Workflow>(_context));
        public IAsyncRepository<WorkflowStep> WorkflowStepRepository => _workflowStepRepository ?? (_workflowStepRepository = new EfRepository<WorkflowStep>(_context));
        #endregion

        public UnitOfWork(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<WorkFlowDbContext>();
            optionsBuilder.UseSqlServer(connectionString);
            _context = new WorkFlowDbContext(optionsBuilder.Options);
            var loggerFactory = new LoggerFactory();
            _logger = loggerFactory.CreateLogger<UnitOfWork>();
        }

        public async Task SaveChanges()
        {
            const string loggerHeader = "UnitOfWork";
            using (var dbContextTransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    await _context.SaveChangesAsync();
                    await dbContextTransaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    await dbContextTransaction.RollbackAsync();
                    await SaveErrorLog(ex);
                    throw;
                }
            }
        }

        public async Task SaveErrorLog(Exception ex)
        {
            try
            {
                int code = 0;
                var w32ex = ex as Win32Exception;
                if (w32ex == null)
                {
                    w32ex = ex.InnerException as Win32Exception;
                }
                if (w32ex != null)
                {
                    code = w32ex.ErrorCode;
                }

                var ErrorLog = new ErrorLog
                {
                    Time = DateTime.UtcNow,
                    Number = code.ToString(),
                    Name = ex.GetType().Name,
                    Description = ex.Message,
                    IsDeleted = false
                };

                var commandText = @"INSERT ErrorLog (Name, Number, Time, Description, IsDeleted)
                                    VALUES (@Name, @Number, @Time, @Description, I@sDeleted)";

                SqlParameter[] parameters = new[]{
                    new SqlParameter("@Name", ErrorLog.Name),
                    new SqlParameter("@Number", ErrorLog.Number),
                    new SqlParameter("@Time", ErrorLog.Time),
                    new SqlParameter("@Description", ErrorLog.Description),
                    new SqlParameter("@IsDeleted", ErrorLog.IsDeleted),
                };
                await _context.Database.ExecuteSqlRawAsync(commandText, parameters);
            }
            catch { }
        }

        #region IDisposable Support  
        private bool _disposedValue = false;

        protected virtual async Task Dispose(bool disposing)
        {
            if (_disposedValue) return;

            if (disposing)
            {
                await _context.DisposeAsync();
            }

            _disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true).Wait();
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
