﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IDocumentTypeService
    {
        Task<ApiResponse<DocumentType>> CreateDocumentType(DocumentTypeResource documentTypeResource);
        Task<ApiResponse<DocumentType>> UpdateDocumentType(long id, DocumentTypeResource documentTypeResource);
        Task<ApiResponse<DocumentType>> DeleteDocumentType(long id, bool removeFromDB = false);
        Task<ApiResponse<DocumentType>> GetDocumentType(long id);
        Task<ApiResponse<QueryResult<DocumentType>>> GetDocumentTypes(QueryResource queryObj);
        Task<ApiResponse<IList<DocumentType>>> ImportFromList(List<string> documentTypeTitles);
    }
}
