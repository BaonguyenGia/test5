﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IDepartmentService
    {
        Task<ApiResponse<Department>> CreateDepartment(DepartmentResource departmentResource);
        Task<ApiResponse<Department>> UpdateDepartment(long id, DepartmentResource departmentResource);
        Task<ApiResponse<Department>> DeleteDepartment(long id, bool removeFromDB = false);
        Task<ApiResponse<Department>> GetDepartment(long id);
        Task<ApiResponse<QueryResultResource<DepartmentResource>>> GetDepartments(QueryResource queryObj);
        Task<ApiResponse<List<DepartmentTreeView>>> GetTreeViewDepartments(QueryResource queryResource);
    }
}
