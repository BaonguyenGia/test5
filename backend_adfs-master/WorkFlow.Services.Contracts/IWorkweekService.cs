﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkweekService
    {
        Task<ApiResponse<Workweek>> CreateWorkweek(WorkweekResource workweekResource);
        Task<ApiResponse<Workweek>> UpdateWorkweek(long id, WorkweekResource workweekResource);
        Task<ApiResponse<Workweek>> DeleteWorkweek(long id, bool removeFromDB = false);
        Task<ApiResponse<Workweek>> GetWorkweek(long id);
        Task<ApiResponse<QueryResult<Workweek>>> GetWorkweeks(QueryResource queryObj);
    }
}
