﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IGroupService
    {
        Task<ApiResponse<Group>> CreateGroup(GroupResource GroupResource);
        Task<ApiResponse<Group>> UpdateGroup(long id, GroupResource GroupResource);
        Task<ApiResponse<Group>> DeleteGroup(long id, bool removeFromDB = false);
        Task<ApiResponse<Group>> GetGroup(long id);
        Task<ApiResponse<QueryResult<Group>>> GetGroups(QueryResource queryObj);
    }
}
