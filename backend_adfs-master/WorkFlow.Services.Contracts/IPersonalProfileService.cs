﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IPersonalProfileService
    {
        Task<ApiResponse<PersonalProfile>> CreatePersonalProfile(PersonalProfileResource personalProfileResource);
        Task<ApiResponse<PersonalProfile>> UpdatePersonalProfile(long id, PersonalProfileResource personalProfileResource);
        Task<ApiResponse<PersonalProfile>> DeletePersonalProfile(long id, bool removeFromDB = false);
        Task<ApiResponse<PersonalProfile>> GetPersonalProfile(long id);
        Task<ApiResponse<QueryResultResource<PersonalProfileResource>>> GetPersonalProfiles(QueryResource queryObj);
        Task SecretUpdatePersonalProfile();
        Task<ApiResponse<PersonalProfile>> UploadPersonalProfileAvatar(long id, IFormFile file);
        Task<ApiResponse<FileStream>> GetPersonalProfileAvatar(long id);
        Task<ApiResponse<FileStream>> GetPersonalProfileAvatar(string email);
    }
}
