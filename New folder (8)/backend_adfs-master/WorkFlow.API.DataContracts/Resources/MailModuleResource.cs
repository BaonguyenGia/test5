﻿using System.Collections.Generic;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class MailModuleResource : EngineEntity
    {
        public string Name { get; set; }
        public int MyProperty { get; set; }
        public ICollection<MailTemplateResource> MailTemplates { get; set; }
    }
}
