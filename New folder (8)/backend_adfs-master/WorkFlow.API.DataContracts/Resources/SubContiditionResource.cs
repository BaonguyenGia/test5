﻿using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class SubContiditionResource : EngineEntity
    {
        [Required]
        public long? WorkflowConditionId { get; set; }
        [Required]
        public string Variable { get; set; }
        [Required]
        public string Conditional { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        public string NexCondition { get; set; }
        [Required]
        public int Order { get; set; }
    }
}
