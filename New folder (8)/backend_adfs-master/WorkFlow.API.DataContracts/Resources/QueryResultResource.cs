using System.Collections.Generic;

namespace WorkFlow.API.DataContracts.Resources
{
    public class QueryResultResource<T>
    {
        public long TotalItems { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}