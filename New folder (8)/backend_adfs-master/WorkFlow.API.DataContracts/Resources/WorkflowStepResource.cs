﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class WorkflowStepResource : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public long? WorkflowId { get; set; }
        public long? AssignmentRuleId { get; set; }
        public int Step { get; set; }
        public int NextStep { get; set; }
        public int ReturnStep { get; set; }
        public string Status { get; set; }
        #region Timing Control
        public int Duration { get; set; }
        public string TimeUnit { get; set; }
        public string EnterDay { get; set; }
        #endregion
        #region Mail & Mail Notifications
        public string MailCC { get; set; }
        public long? EmailAddInfo { get; set; }
        public long? EmailNext { get; set; }
        public long? EmailRecall { get; set; }
        public long? EmailReplace { get; set; }
        public long? EmailReject { get; set; }
        public long? EmailApprove { get; set; }
        public long? EmailPrevious { get; set; }
        public long? EmailRequestIdea { get; set; }
        public long? EmailAuthor { get; set; }
        public long? EmailNextCC { get; set; }
        #endregion
        #region Button
        public string LabelAddInfo { get; set; }
        public string LabelSave { get; set; }
        public string LabelNext { get; set; }
        public string LabelReturn { get; set; }
        public string LabelApprove { get; set; }
        public string LabelReject { get; set; }
        public string LabelRecall { get; set; }
        public string LabelReplace { get; set; }
        public string LabelIdea { get; set; }
        public string LabelShare { get; set; }
        public string LabelExit { get; set; }
        public string LabelPrint { get; set; }
        #endregion
        #region  Foreign
        public WorkflowResource Workflow { get; set; }
        public AssignmentRuleResource AssignmentRule { get; set; }
        public ICollection<CheckListResource> CheckLists { get; set; }
        #endregion
    }
}
