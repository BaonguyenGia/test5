﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class GroupResource : EngineEntity
    {
        public string Title { get; set; }
    }
}