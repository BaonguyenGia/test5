﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class MailTemplateResource : EngineEntity
    {
        public string Name { get; set; }
        public long? MailModuleId { get; set; }
        public string Subject { get; set; }
        public string SubjectEN { get; set; }
        public string SubjectParameters { get; set; }
        public string Content { get; set; }
        public string ContentParameters { get; set; }
    }
}
