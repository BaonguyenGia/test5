﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class CheckList : EngineEntity
    {
        public string Title { get; set; }
        [ForeignKey("WorkflowStep")]
        public long? WorkflowStepId { get; set; }
        public long? TypeId { get; set; }
        public bool IsRequired { get; set; }
        public bool IsQRCode { get; set; }
        public bool IsSignature { get; set; }
        #region  Foreign
        [ForeignKey("TypeId")]
        public DocumentType DocumentType { get; set; }
        #endregion
    }
}
