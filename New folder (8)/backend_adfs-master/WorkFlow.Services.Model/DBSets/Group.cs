﻿using System.Collections.Generic;

namespace WorkFlow.Services.Models
{
    public class Group : EngineEntity
    {
        public string Title { get; set; }
        #region Foreign
        public ICollection<PersonalProfile> PersonalProfiles { get; set; }
        #endregion
    }
}
