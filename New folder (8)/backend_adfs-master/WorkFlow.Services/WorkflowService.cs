﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class WorkflowService : IWorkflowService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<WorkflowService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public WorkflowService(IMapper mapper, ILogger<WorkflowService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<Workflow>> CreateWorkflow(WorkflowResource workflowResource)
        {
            const string loggerHeader = "CreateWorkflow";

            var apiResponse = new ApiResponse<Workflow>();
            Workflow workflow = _mapper.Map<WorkflowResource, Workflow>(workflowResource);

            _logger.LogDebug($"{loggerHeader} - Start to addWorkflow: {JsonConvert.SerializeObject(workflow)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    workflow.CreatedBy = _httpContextHelper.GetCurrentUser();
                    workflow.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.WorkflowRepository.Add(workflow);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add newWorkflow successfully with Id: {workflow.Id}");
                    apiResponse.Data = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == workflow.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Workflow>> UpdateWorkflow(long id, WorkflowResource workflowResource)
        {
            const string loggerHeader = "UpdateWorkflow";
            var apiResponse = new ApiResponse<Workflow>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workflow = await unitOfWork.WorkflowRepository.FindFirst(predicate: d => d.Id == id);
                    workflow = _mapper.Map<WorkflowResource, Workflow>(workflowResource, workflow);
                    _logger.LogDebug($"{loggerHeader} - Start to updateWorkflow: {JsonConvert.SerializeObject(workflow)}");

                    workflow.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workflow.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkflowRepository.Update(workflow);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - UpdateWorkflow successfully with Id: {workflow.Id}");

                    apiResponse.Data = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == workflow.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Workflow>> DeleteWorkflow(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteWorkflow";

            var apiResponse = new ApiResponse<Workflow>();

            _logger.LogDebug($"{loggerHeader} - Start to deleteWorkflow with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workflow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.WorkflowRepository.Remove(workflow);
                    }
                    else
                    {
                        workflow.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workflow.IsDeleted = true;
                        workflow.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkflowRepository.Update(workflow);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - DeleteWorkflow successfully with Id: {workflow.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<Workflow>> GetWorkflow(long id)
        {
            const string loggerHeader = "UpdateWorkflow";

            var apiResponse = new ApiResponse<Workflow>();

            _logger.LogDebug($"{loggerHeader} - Start to getWorkflow with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    apiResponse.Data = await unitOfWork.WorkflowRepository.FindFirst(predicate: d => d.Id == id,
                                                                                    include: source => source.Include(d => d.Properties)
                                                                                                                .ThenInclude(p => p.Setting)
                                                                                                                    .ThenInclude(s => s.Choices)
                                                                                                              .Include(d => d.Category));
                    _logger.LogDebug($"{loggerHeader} - GetWorkflow successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<Workflow>>> GetWorkflows(QueryResource queryObj)
        {
            const string loggerHeader = "GetWorkflows";

            var apiResponse = new ApiResponse<QueryResult<Workflow>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to getWorkflows with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Workflow, object>>>()
                    {
                        ["title"] = s => s.Title,
                        ["code"] = s => s.Code
                    };

                    var query = await unitOfWork.WorkflowRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%"))
                                                                                            && (!queryObj.CategoryId.HasValue || d.CategoryId == queryObj.CategoryId.Value)
                                                                                            && (String.IsNullOrEmpty(queryObj.CreatePermission) || d.CreatePermission.Contains(queryObj.CreatePermission))
                                                                                            && (String.IsNullOrEmpty(queryObj.SeenPermission) || d.CreatePermission.Contains(queryObj.SeenPermission)),
                                                                        include: source => source.Include(d => d.Properties).ThenInclude(p => p.DefaultType)
                                                                                                  .Include(d => d.Properties).ThenInclude(p => p.Setting).ThenInclude(s => s.Choices),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - GetWorkflows successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
