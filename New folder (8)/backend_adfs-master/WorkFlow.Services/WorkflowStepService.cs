﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class WorkflowStepService : IWorkflowStepService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<WorkflowStepService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public WorkflowStepService(IMapper mapper, ILogger<WorkflowStepService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<List<WorkflowStep>>> CreateWorkflowSteps(List<WorkflowStepResource> workflowStepResources)
        {
            const string loggerHeader = "CreateWorkflowSteps";
            var apiResponse = new ApiResponse<List<WorkflowStep>>();

            _logger.LogDebug($"{loggerHeader} - Start to add WorkflowSteps: {JsonConvert.SerializeObject(workflowStepResources)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    List<WorkflowStep> workflowSteps = new();
                    foreach (var workflowStepResource in workflowStepResources)
                    {
                        var workflowStep = _mapper.Map<WorkflowStepResource, WorkflowStep>(workflowStepResource);
                        workflowStep.CreatedBy = _httpContextHelper.GetCurrentUser();
                        workflowStep.CreatedTime = DateTime.UtcNow;
                        workflowSteps.Add(workflowStep);
                    }

                    await unitOfWork.WorkflowStepRepository.AddRange(workflowSteps);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new WorkflowSteps successfully");

                    var lsID = workflowSteps.Where(w => w.Id > 0).Select(w => w.Id).ToList();
                    apiResponse.Data = (await unitOfWork.WorkflowStepRepository.FindAll(d => lsID.Contains(d.Id))).Items.ToList();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowStep>> CreateWorkflowStep(WorkflowStepResource workflowStepResource)
        {
            const string loggerHeader = "CreateWorkflowStep";

            var apiResponse = new ApiResponse<WorkflowStep>();
            WorkflowStep workflowStep = _mapper.Map<WorkflowStepResource, WorkflowStep>(workflowStepResource);

            _logger.LogDebug($"{loggerHeader} - Start to add WorkflowStep: {JsonConvert.SerializeObject(workflowStep)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    workflowStep.CreatedBy = _httpContextHelper.GetCurrentUser();
                    workflowStep.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.WorkflowStepRepository.Add(workflowStep);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new WorkflowStep successfully with Id: {workflowStep.Id}");
                    apiResponse.Data = await unitOfWork.WorkflowStepRepository.FindFirst(d => d.Id == workflowStep.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowStep>> UpdateWorkflowStep(long id, WorkflowStepResource workflowStepResource)
        {
            const string loggerHeader = "UpdateWorkflowStep";
            var apiResponse = new ApiResponse<WorkflowStep>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var workflowStep = await unitOfWork.WorkflowStepRepository.FindFirst(d => d.Id == id);
                    workflowStep = _mapper.Map<WorkflowStepResource, WorkflowStep>(workflowStepResource, workflowStep);
                    _logger.LogDebug($"{loggerHeader} - Start to update WorkflowStep: {JsonConvert.SerializeObject(workflowStep)}");

                    workflowStep.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workflowStep.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkflowStepRepository.Update(workflowStep);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update WorkflowStep successfully with Id: {workflowStep.Id}");

                    apiResponse.Data = await unitOfWork.WorkflowStepRepository.FindFirst(d => d.Id == workflowStep.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowStep>> DeleteWorkflowStep(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteWorkflowStep";

            var apiResponse = new ApiResponse<WorkflowStep>();

            _logger.LogDebug($"{loggerHeader} - Start to delete WorkflowStep with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var workflowStep = await unitOfWork.WorkflowStepRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.WorkflowStepRepository.Remove(workflowStep);
                    }
                    else
                    {
                        workflowStep.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workflowStep.IsDeleted = true;
                        workflowStep.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkflowStepRepository.Update(workflowStep);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete WorkflowStep successfully with Id: {workflowStep.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowStep>> GetWorkflowStep(long id)
        {
            const string loggerHeader = "UpdateWorkflowStep";

            var apiResponse = new ApiResponse<WorkflowStep>();

            _logger.LogDebug($"{loggerHeader} - Start to get WorkflowStep with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    apiResponse.Data = await unitOfWork.WorkflowStepRepository.FindFirst(d => d.Id == id);
                    _logger.LogDebug($"{loggerHeader} - Get WorkflowStep successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<WorkflowStep>>> GetWorkflowSteps(QueryResource queryObj)
        {
            const string loggerHeader = "GetWorkflowSteps";

            var apiResponse = new ApiResponse<QueryResult<WorkflowStep>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get WorkflowSteps with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<WorkflowStep, object>>>()
                    {
                        ["title"] = s => s.Title,
                    };

                    var query = await unitOfWork.WorkflowStepRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: source => source.Include(d => d.AssignmentRule).Include(d => d.CheckLists),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - Get WorkflowSteps successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
