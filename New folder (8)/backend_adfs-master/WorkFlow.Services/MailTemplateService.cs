﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class MailTemplateService : IMailTemplateService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<MailTemplateService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public MailTemplateService(IMapper mapper, ILogger<MailTemplateService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<MailTemplate>> CreateMailTemplate(MailTemplateResource mailTemplateResource)
        {
            const string loggerHeader = "CreateMailTemplate";

            var apiResponse = new ApiResponse<MailTemplate>();
            MailTemplate mailTemplate = _mapper.Map<MailTemplateResource, MailTemplate>(mailTemplateResource);

            _logger.LogDebug($"{loggerHeader} - Start to add MailTemplate: {JsonConvert.SerializeObject(mailTemplate)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    mailTemplate.CreatedBy = _httpContextHelper.GetCurrentUser();
                    mailTemplate.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.MailTemplateRepository.Add(mailTemplate);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new MailTemplate successfully with Id: {mailTemplate.Id}");
                    apiResponse.Data = await unitOfWork.MailTemplateRepository.FindFirst(d => d.Id == mailTemplate.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<MailTemplate>> UpdateMailTemplate(long id, MailTemplateResource mailTemplateResource)
        {
            const string loggerHeader = "UpdateMailTemplate";
            var apiResponse = new ApiResponse<MailTemplate>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var mailTemplate = await unitOfWork.MailTemplateRepository.FindFirst(d => d.Id == id);
                    mailTemplate = _mapper.Map<MailTemplateResource, MailTemplate>(mailTemplateResource, mailTemplate);
                    _logger.LogDebug($"{loggerHeader} - Start to update MailTemplate: {JsonConvert.SerializeObject(mailTemplate)}");

                    mailTemplate.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    mailTemplate.LastModified = DateTime.UtcNow;
                    unitOfWork.MailTemplateRepository.Update(mailTemplate);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update MailTemplate successfully with Id: {mailTemplate.Id}");

                    apiResponse.Data = await unitOfWork.MailTemplateRepository.FindFirst(d => d.Id == mailTemplate.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<MailTemplate>> DeleteMailTemplate(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteMailTemplate";

            var apiResponse = new ApiResponse<MailTemplate>();

            _logger.LogDebug($"{loggerHeader} - Start to delete MailTemplate with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var mailTemplate = await unitOfWork.MailTemplateRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.MailTemplateRepository.Remove(mailTemplate);
                    }
                    else
                    {
                        mailTemplate.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        mailTemplate.IsDeleted = true;
                        mailTemplate.LastModified = DateTime.UtcNow;
                        unitOfWork.MailTemplateRepository.Update(mailTemplate);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete MailTemplate successfully with Id: {mailTemplate.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<MailTemplate>> GetMailTemplate(long id)
        {
            const string loggerHeader = "UpdateMailTemplate";

            var apiResponse = new ApiResponse<MailTemplate>();

            _logger.LogDebug($"{loggerHeader} - Start to get MailTemplate with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    apiResponse.Data = await unitOfWork.MailTemplateRepository.FindFirst(d => d.Id == id);
                    _logger.LogDebug($"{loggerHeader} - Get MailTemplate successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<MailTemplate>>> GetMailTemplates(QueryResource queryObj)
        {
            const string loggerHeader = "GetMailTemplates";

            var apiResponse = new ApiResponse<QueryResult<MailTemplate>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get MailTemplates with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<MailTemplate, object>>>()
                    {
                        ["name"] = s => s.Name
                    };

                    var query = await unitOfWork.MailTemplateRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Name, $"%{queryObj.Name}%")),
                                                                        include: null,
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - Get MailTemplates successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
