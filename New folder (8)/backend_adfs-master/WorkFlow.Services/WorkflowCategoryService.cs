﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class WorkflowCategoryService : IWorkflowCategoryService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<WorkflowCategoryService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public WorkflowCategoryService(IMapper mapper, ILogger<WorkflowCategoryService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<WorkflowCategory>> CreateWorkflowCategory(WorkflowCategoryResource workflowCategoryResource)
        {
            const string loggerHeader = "CreateWorkflowCategory";

            var apiResponse = new ApiResponse<WorkflowCategory>();
            WorkflowCategory workflowCategory = _mapper.Map<WorkflowCategoryResource, WorkflowCategory>(workflowCategoryResource);

            _logger.LogDebug($"{loggerHeader} - Start to add WorkflowCategory: {JsonConvert.SerializeObject(workflowCategory)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    workflowCategory.CreatedBy = _httpContextHelper.GetCurrentUser();
                    workflowCategory.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.WorkflowCategoryRepository.Add(workflowCategory);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new WorkflowCategory successfully with Id: {workflowCategory.Id}");
                    apiResponse.Data = await unitOfWork.WorkflowCategoryRepository.FindFirst(d => d.Id == workflowCategory.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowCategory>> UpdateWorkflowCategory(long id, WorkflowCategoryResource workflowCategoryResource)
        {
            const string loggerHeader = "UpdateWorkflowCategory";
            var apiResponse = new ApiResponse<WorkflowCategory>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var workflowCategory = await unitOfWork.WorkflowCategoryRepository.FindFirst(d => d.Id == id);
                    workflowCategory = _mapper.Map<WorkflowCategoryResource, WorkflowCategory>(workflowCategoryResource, workflowCategory);
                    _logger.LogDebug($"{loggerHeader} - Start to update WorkflowCategory: {JsonConvert.SerializeObject(workflowCategory)}");

                    workflowCategory.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workflowCategory.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkflowCategoryRepository.Update(workflowCategory);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update WorkflowCategory successfully with Id: {workflowCategory.Id}");

                    apiResponse.Data = await unitOfWork.WorkflowCategoryRepository.FindFirst(d => d.Id == workflowCategory.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowCategory>> DeleteWorkflowCategory(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteWorkflowCategory";

            var apiResponse = new ApiResponse<WorkflowCategory>();

            _logger.LogDebug($"{loggerHeader} - Start to delete WorkflowCategory with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workflowCategory = await unitOfWork.WorkflowCategoryRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.WorkflowCategoryRepository.Remove(workflowCategory);
                    }
                    else
                    {
                        workflowCategory.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workflowCategory.IsDeleted = true;
                        workflowCategory.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkflowCategoryRepository.Update(workflowCategory);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete WorkflowCategory successfully with Id: {workflowCategory.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowCategory>> GetWorkflowCategory(long id)
        {
            const string loggerHeader = "UpdateWorkflowCategory";

            var apiResponse = new ApiResponse<WorkflowCategory>();

            _logger.LogDebug($"{loggerHeader} - Start to get WorkflowCategory with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    apiResponse.Data = await unitOfWork.WorkflowCategoryRepository.FindFirst(d => d.Id == id);
                    _logger.LogDebug($"{loggerHeader} - Get WorkflowCategory successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResult<WorkflowCategory>>> GetWorkflowCategories(QueryResource queryObj)
        {
            const string loggerHeader = "GetWorkflowCategories";

            var apiResponse = new ApiResponse<QueryResult<WorkflowCategory>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get WorkflowCategorys with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<WorkflowCategory, object>>>()
                    {
                        ["title"] = s => s.Title
                    };

                    var query = await unitOfWork.WorkflowCategoryRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: null,
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = query;
                    _logger.LogDebug($"{loggerHeader} - Get WorkflowCategorys successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
