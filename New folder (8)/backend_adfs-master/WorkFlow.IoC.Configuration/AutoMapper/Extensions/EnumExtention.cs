﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace WorkFlow.IoC.Configuration.AutoMapper.Extensions
{
    public static class EnumExtention
    {
        public static string ToDictionary(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());
            DescriptionAttribute attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }
    }
}
