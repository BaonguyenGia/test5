﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IMailModuleService
    {
        Task<ApiResponse<MailModule>> CreateMailModule(MailModuleResource mailModuleResource);
        Task<ApiResponse<MailModule>> UpdateMailModule(long id, MailModuleResource mailModuleResource);
        Task<ApiResponse<MailModule>> DeleteMailModule(long id, bool removeFromDB = false);
        Task<ApiResponse<MailModule>> GetMailModule(long id);
        Task<ApiResponse<QueryResult<MailModule>>> GetMailModules(QueryResource queryObj);
    }
}
