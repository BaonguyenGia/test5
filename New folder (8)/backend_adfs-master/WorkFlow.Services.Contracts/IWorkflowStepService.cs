﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkflowStepService
    {
        Task<ApiResponse<WorkflowStep>> CreateWorkflowStep(WorkflowStepResource workflowStepResource);
        Task<ApiResponse<List<WorkflowStep>>> CreateWorkflowSteps(List<WorkflowStepResource> workflowStepResources);
        Task<ApiResponse<WorkflowStep>> UpdateWorkflowStep(long id, WorkflowStepResource workflowStepResource);
        Task<ApiResponse<WorkflowStep>> DeleteWorkflowStep(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkflowStep>> GetWorkflowStep(long id);
        Task<ApiResponse<QueryResult<WorkflowStep>>> GetWorkflowSteps(QueryResource queryObj);
    }
}
