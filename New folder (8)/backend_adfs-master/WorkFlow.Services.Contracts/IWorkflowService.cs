﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkflowService
    {
        Task<ApiResponse<Workflow>> CreateWorkflow(WorkflowResource workflowResource);
        Task<ApiResponse<Workflow>> UpdateWorkflow(long id, WorkflowResource workflowResource);
        Task<ApiResponse<Workflow>> DeleteWorkflow(long id, bool removeFromDB = false);
        Task<ApiResponse<Workflow>> GetWorkflow(long id);
        Task<ApiResponse<QueryResult<Workflow>>> GetWorkflows(QueryResource queryObj);
    }
}
