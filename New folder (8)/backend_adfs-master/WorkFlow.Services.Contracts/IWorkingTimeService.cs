﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkingTimeService
    {
        Task<ApiResponse<WorkingTime>> CreateWorkingTime(WorkingTimeResource workingTimeResource);
        Task<ApiResponse<WorkingTime>> UpdateWorkingTime(long id, WorkingTimeResource workingTimeResource);
        Task<ApiResponse<WorkingTime>> DeleteWorkingTime(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkingTime>> GetWorkingTime(long id);
        Task<ApiResponse<QueryResult<WorkingTime>>> GetWorkingTimes(QueryResource queryObj);
    }
}
