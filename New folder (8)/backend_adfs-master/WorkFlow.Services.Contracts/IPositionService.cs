﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IPositionService
    {
        Task<ApiResponse<Position>> CreatePosition(PositionResource PositionResource);
        Task<ApiResponse<Position>> UpdatePosition(long id, PositionResource PositionResource);
        Task<ApiResponse<Position>> DeletePosition(long id, bool removeFromDB = false);
        Task<ApiResponse<Position>> GetPosition(long id);
        Task<ApiResponse<QueryResult<Position>>> GetPositions(QueryResource queryObj);
    }
}
