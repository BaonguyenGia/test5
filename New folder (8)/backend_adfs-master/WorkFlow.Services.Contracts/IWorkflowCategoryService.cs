﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkflowCategoryService
    {
        Task<ApiResponse<WorkflowCategory>> CreateWorkflowCategory(WorkflowCategoryResource WorkflowCategoryResource);
        Task<ApiResponse<WorkflowCategory>> UpdateWorkflowCategory(long id, WorkflowCategoryResource WorkflowCategoryResource);
        Task<ApiResponse<WorkflowCategory>> DeleteWorkflowCategory(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkflowCategory>> GetWorkflowCategory(long id);
        Task<ApiResponse<QueryResult<WorkflowCategory>>> GetWorkflowCategories(QueryResource queryObj);
    }
}
