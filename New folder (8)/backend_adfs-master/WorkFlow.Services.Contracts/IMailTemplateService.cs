﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.Services.Contracts
{
    public interface IMailTemplateService
    {
        Task<ApiResponse<MailTemplate>> CreateMailTemplate(MailTemplateResource mailTemplateResource);
        Task<ApiResponse<MailTemplate>> UpdateMailTemplate(long id, MailTemplateResource mailTemplateResource);
        Task<ApiResponse<MailTemplate>> DeleteMailTemplate(long id, bool removeFromDB = false);
        Task<ApiResponse<MailTemplate>> GetMailTemplate(long id);
        Task<ApiResponse<QueryResult<MailTemplate>>> GetMailTemplates(QueryResource queryObj);
    }
}
