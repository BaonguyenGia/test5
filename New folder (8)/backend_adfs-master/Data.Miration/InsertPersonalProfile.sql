/****** Script for SelectTopNRows command from SSMS  ******/
INSERT INTO [WorkFlow].[dbo].[PersonalProfile]( 
		[AccountId]
      ,[AccountName]
      ,[Name]
      ,[FullName]
      ,[DepartmentId]
      ,[Gender]
      ,[BirthDay]
      ,[Address]
      ,[StaffId]
      ,[DateOfHire]
      ,[Mobile]
      ,[Email]
      ,[SignatureUserName]
      ,[SignaturePassword]
      ,[EnableSignature]
      ,[UserStatus]
      ,[CreatedBy]
      ,[CreatedTime]
      ,[ModifiedBy]
      ,[IsDeleted])

SELECT pp.[AccountId]
      ,pp.[AccountName]
      ,pp.[Name]
      ,pp.[FullNameVn]
	  ,de.Id
	  ,pp.[Gender]
      ,pp.[BirthDay]
      ,pp.[Address]
      ,pp.[StaffId]
      ,pp.[DateOfHire]
      ,pp.[Mobile]
      ,pp.[Email]
	  ,pp.[SignatureUserName]
      ,pp.[SignaturePassword]
      ,pp.[EnableSignature]
	  ,pp.[UserStatus]
      ,pp.Manager
      ,GETUTCDATE()
	  ,pp.Position
      ,0
 FROM [DATA_EOFFICE_BG_BCMWF].[dbo].[PersonalProfile] pp
 INNER JOIN [WorkFlow].[dbo].[Department] de on de.Title = pp.Department
