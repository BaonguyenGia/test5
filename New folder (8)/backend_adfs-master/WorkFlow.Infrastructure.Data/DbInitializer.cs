﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using WorkFlow.Services.Models;

namespace WorkFlow.Infrastructure.Data
{
    public class DbInitializer
    {
        public static async Task Initialize(WorkFlowDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.MailModules.Any())
            {
                await Task.CompletedTask; //DB has been seeded
            }
            else
            {
                var mailModules = new MailModule[]
                {
                    new MailModule{Name="Văn bản đến", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new MailModule{Name="Văn bản đi", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new MailModule{Name="Công việc", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new MailModule{Name="Tin nhắn nội bộ", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new MailModule{Name="Lịch", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new MailModule{Name="Tin tức", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new MailModule{Name="Quy trình", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"}
                };
                await context.MailModules.AddRangeAsync(mailModules);
                await context.SaveChangesAsync();

                var mailTemplates = new MailTemplate[]
                {
                    new MailTemplate{Name="Quy trình - Bổ sung thông tin",Subject=@"Phiếu ""{0}"" đã được yêu cầu bổ sung thông tin",
                    SubjectEN = "Proposal request {0}) has been request for information",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)<br><br>&#160;Gửi Anh/ Chị, <br>&#160;<br>Phiếu &quot;&#123;0&#125;&quot; của anh/chị được yêu cầu bổ 
                    sung thông tin.<br></div><div>Nội dung phiếu Như sau&#58;</div><div>&#160;</div><div>&#123;3&#125;&#160;</div><div><br>Để xem chi tiết nội dung, anh/chị 
                    vui lòng click &#123;1&#125;<br>&#160;<br>Trân trọng cảm ơn!<br>&#160;<br>******************************************************************************************<br>
                    &#160;<br>Dear Sir/ Madams,<br>&#160;<br>Proposal request &quot;&#123;0&#125;&quot; has been request for information<br>&#160;<br>For details, 
                    please kindly click &#123;2&#125;<br>&#160;<br>Thank you!</div><div>&#160;</div><div>****************************************************************************
                    **************</div>",
                    ContentParameters = @"Title;#""tại đây""(link);#""here""(link);WorkflowContent",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Chia sẻ",Subject=@"Phiếu ""{0}"" được chia sẻ",
                    SubjectEN = "The proposal {0} has been shared",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)</div><div>&#160;</div><div>Kính gửi Anh/ Chị,</div><div>&#160;</div><div>Phiếu đề nghị vừa được &#123;0&#125; chia sẻ 
                    tới anh/chị, chi tiết gồm&#58;</div><div>&#160;</div><div>Mã yêu cầu&#58; &#123;1&#125;<br>Nội dung&#58; &#123;2&#125;</div><div>&#160;</div><div>Để xem chi tiết nội dung, vui lòng click 
                    &#123;3&#125;<br>&#160;<br>Trân trọng cảm ơn!</div><div>&#160;</div><div>******************************************************************************************</div><div>&#160;
                    </div><div>Dear Sirs/ Madams,<br>&#160;<br>Proposal was shared by &#123;0&#125; to you</div><div>&#160;</div><div>Code request&#58; &#123;1&#125;<br>Content&#58; &#123;2&#125;<br>&#160;
                    <br>For detail, please kindly click &#123;4&#125;<br>&#160;<br>Thank you!</div><div>&#160;</div><div>******************************************************************************************
                    </div><div></div>",
                    ContentParameters = @"currentuser;#Title;#WorkflowContent;#""tại đây""(link);#""here""(link)",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Đề nghị được phê duyệt",Subject=@"Phiếu ""{0}"" đã được phê duyệt",
                    SubjectEN = "The proposal {0} has been approved",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)</div><div><br>Kính gửi Anh/ Chị,</div><div>&#160;</div><div>Phiếu đề nghị của anh/chị đã được phê duyệt, chi tiết 
                    gồm&#58;</div><div>&#160;</div><div>Mã yêu cầu&#58; &#123;0&#125;<br>Nội dung&#58; &#123;1&#125;<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;3&#125;</div><div>&#160;</div>
                    <div>Trân trọng cảm ơn!</div><div><br>******************************************************************</div><div><br>Dear Sir/ Madams,</div><div>&#160;</div><div>Proposal has been 
                    accepted&#58;</div><div>&#160;</div><div>Code request&#58; &#123;0&#125;<br>Content&#58; &#123;1&#125;<br>&#160;<br>For details, please kindly click &#123;4&#125;</div><div>&#160;
                    <br>Thank you!</div><div>&#160;</div><div>******************************************************************</div><div></div>",
                    ContentParameters = @"Title;#WorkflowContent;#currentuser;#""tại đây""(link);#""here""(link)",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Step CC/Group CC",Subject=@"Phiếu ""{0}"" đã được phê duyệt",
                    SubjectEN = "The proposal {0} has been approved",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English) Kính gửi Anh/ Chị, Phiếu đề nghị đã được phê duyệt, chi tiết gồm&#58; Mã yêu cầu&#58; &#123;0&#125; 
                    Nội dung&#58; &#123;1&#125; Để xem chi tiết nội dung, vui lòng click &#123;3&#125; Trân trọng cảm ơn! ****************************************************************** 
                    Dear Sir/ Madams, Proposal has been accepted&#58; Code request&#58; &#123;0&#125; Content&#58; &#123;1&#125; For details, please kindly click &#123;4&#125;
                    Thank you! ******************************************************************</div>",
                    ContentParameters = "",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Thông báo bình luận",Subject=@"Phiếu đề nghị ""{0}""",
                    SubjectEN = "The proposal {0}",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div align=""left"">&#160;(Please scroll down for English)</div><div align=""left""><br>Kính gửi Anh/ Chị,</div><div align=""left"">&#160;</div>
                    \<div align=""left"">Phiếu đề nghị của anh/chị đã được phê duyệt, chi tiết gồm&#58;</div><div align=""left"">&#160;</div><div align=""left"">Mã yêu cầu&#58; &#123;0&#125;
                    <br>Nội dung&#58; &#123;1&#125;<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;3&#125;</div><div align=""left"">&#160;</div><div align=""left"">Trân trọng cảm ơn!</div>
                    <div align=""left""><br>******************************************************************</div><div align=""left""><br>Dear Sir/ Madams,</div><div align=""left"">&#160;
                    </div><div align=""left"">Proposal has been accepted&#58;</div><div align=""left"">&#160;</div><div align=""left"">Code request&#58; &#123;0&#125;<br>Content&#58; &#123;1&#125;
                    <br>&#160;<br>For details, please kindly click &#123;4&#125;</div><div align=""left"">&#160;<br>Thank you!</div><div align=""left"">&#160;</div><div align=""left"">
                    ******************************************************************<br></div>",
                    ContentParameters = "",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Thông báo bình luận",Subject=@"Phiếu đề nghị ""{0}""",
                    SubjectEN = "The proposal {0}",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div align=""left"">&#160;(Please scroll down for English)</div><div align=""left""><br>Kính gửi Anh/ Chị,</div><div align=""left"">&#160;</div>
                    \<div align=""left"">Phiếu đề nghị của anh/chị đã được phê duyệt, chi tiết gồm&#58;</div><div align=""left"">&#160;</div><div align=""left"">Mã yêu cầu&#58; &#123;0&#125;
                    <br>Nội dung&#58; &#123;1&#125;<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;3&#125;</div><div align=""left"">&#160;</div><div align=""left"">Trân trọng cảm ơn!</div>
                    <div align=""left""><br>******************************************************************</div><div align=""left""><br>Dear Sir/ Madams,</div><div align=""left"">&#160;
                    </div><div align=""left"">Proposal has been accepted&#58;</div><div align=""left"">&#160;</div><div align=""left"">Code request&#58; &#123;0&#125;<br>Content&#58; &#123;1&#125;
                    <br>&#160;<br>For details, please kindly click &#123;4&#125;</div><div align=""left"">&#160;<br>Thank you!</div><div align=""left"">&#160;</div><div align=""left"">
                    ******************************************************************<br></div>",
                    ContentParameters = "",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Thông báo hoàn tất",Subject=@"Phiếu đề nghị ""{0}"" đã hoàn tất",
                    SubjectEN = "The proposal {0} finished",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div align=""left"">&#160;(Please scroll down for English)</div><div align=""left""><br>Kính gửi Anh/ Chị,</div><div align=""left"">&#160;</div>
                    <div align=""left"">Phiếu đề nghị của anh/chị đã được phê duyệt, chi tiết gồm&#58;</div><div align=""left"">&#160;</div><div align=""left"">Mã yêu cầu&#58; &#123;0&#125;
                    <br>Nội dung&#58; &#123;1&#125;<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;3&#125;</div><div align=""left"">&#160;</div><div align=""left"">
                    Trân trọng cảm ơn!</div><div align=""left""><br>******************************************************************</div><div align=""left""><br>Dear Sir/ Madams,
                    </div><div align=""left"">&#160;</div><div align=""left"">Proposal has been accepted&#58;</div><div align=""left"">&#160;</div><div align=""left"">
                    Code request&#58; &#123;0&#125;<br>Content&#58; &#123;1&#125;<br>&#160;<br>For details, please kindly click &#123;4&#125;</div><div align=""left"">&#160;
                    <br>Thank you!</div><div align=""left"">&#160;</div><div align=""left"">******************************************************************<br></div>",
                    ContentParameters = "",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Thông báo từ chối",Subject=@"Phiếu đề nghị ""{0}"" đã bị từ chối",
                    SubjectEN = "The proposal {0} has been rejected",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)<br>&#160;<br>Kính gửi Anh/ Chị, <br>&#160;<br>Phiếu đề nghị &quot;&#123;0&#125;&quot; của anh/chị vừa bị từ 
                    chối.<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;1&#125;<br>&#160;<br>Trân trọng cảm ơn!<br>&#160;<br>***************************************
                    ***************************************************<br>&#160;<br>Dear Sir/ Madams,<br>&#160;<br>Proposal request &#123;0&#125;) has been rejected for information<br>
                    &#160;<br>For details, please kindly click &#123;2&#125;<br>&#160;<br>Thank you!</div>",
                    ContentParameters = "",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Thu hồi",Subject=@"Phiếu đề nghị ""{0}"" được thu hồi",
                    SubjectEN = "The proposal {0} was been recalled",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)<br>&#160;<br>Kính gửi Anh/ Chị, <br>&#160;<br>Phiếu đề nghị &quot;&#123;0&#125;&quot; 
                    của anh/chị vừa được thu hồi.<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;1&#125;<br>&#160;<br>Trân trọng cảm ơn!
                    <br>&#160;<br>****************************************************************</div><div>&#160;</div><div>Dear Sirs/ Madams,<br>&#160;
                    <br>The proposal &quot;&#123;0&#125;&quot; was recalled<br>&#160;<br>For detail, please kindly click &#123;2&#125;<br>&#160;<br>Thank you!</div>",
                    ContentParameters = @"Title;#""tại đây""(link);#""here""(link)",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                    new MailTemplate{Name="Quy trình - Từ chối",Subject=@"Phiếu đề nghị ""{0}"" đã bị từ chối",
                    SubjectEN = "The proposal {0} has been rejected",
                    SubjectParameters = "WorkflowContent",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)<br>&#160;<br>Kính gửi Anh/ Chị, <br>&#160;<br>Phiếu đề nghị &quot;&#123;0&#125;&quot; của anh/chị 
                    vừa bị từ chối.<br>&#160;<br>Để xem chi tiết nội dung, vui lòng click &#123;1&#125;<br>&#160;<br>Trân trọng cảm ơn!<br>&#160;<br>********************************
                    **********************************************************<br>&#160;<br>Dear Sir/ Madams,<br>&#160;<br>Proposal request &#123;0&#125;) has been rejected for information
                    <br>&#160;<br>For details, please kindly click &#123;2&#125;<br>&#160;<br>Thank you!</div>",
                    ContentParameters = @"Title;#""tại đây""(link);#""here""(link)",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},

                     new MailTemplate{Name="Quy trình - Yêu cầu phê duyệt",Subject=@"Yêu cầu xử lý ""{0}""###{1}###{2}",
                    SubjectEN = @"Request approval for Proposal ""{0}""###{1}###{2}",
                    SubjectParameters = "WorkflowContent;#ID;#Workflow_x003a_ID",
                    MailModuleId = (await context.MailModules.FirstOrDefaultAsync(p=>p.Name=="Quy trình")).Id,
                    Content = @"<div>(Please scroll down for English)<br>&#160;<br>Kính gửi Anh/ Chị, <br>&#160;<br>Yêu cầu phê duyệt phiếu đề nghị của anh/chị vừa được gửi đến,
                    chi tiết bao gồm&#58;</div><div>&#160;</div><div>Mã yêu cầu&#58; &#123;0&#125;</div><div>Nội dung&#58; &#123;1&#125;</div><div>Người yêu cầu&#58; &#123;2&#125;<br>&#160;<br>
                    Để phê duyệt, anh/chị vui lòng click &#123;3&#125;<br>&#160;<br>Anh/chị có thể phê duyệt trên điện thoại&#160; bằng cách reply lại email này theo hướng dẫn dưới đây (lưu ý chức năng này
                    chỉ dành cho lãnh đạo cao cấp)&#58;<br>- Nếu phê duyệt&#58; reply “<font color=""#00ab4f""><strong>Đồng ý</strong></font>” hoặc “<font color=""#00ab4f""><strong>Yes</strong></font>” 
                    hoặc “<font color=""#00ab4f""><strong>Approve</strong></font>”, <br>- Nếu từ chối&#58; Reply <font color=""#ad9e6e""><strong>“Không đồng ý”</strong></font> hoặc <font color=""#ad9e6e"">
                    <strong>“No”</strong></font> hoặc <font color=""#ad9e6e""><strong>“Reject”</strong></font> <br>- Nếu có ý kiến, nhập Ý kiến của anh/chị vào dòng kế tiếp sau khi nhập phê duyệt hoặc từ chối.
                    <br><br>Trân trọng cảm ơn và chúc anh chị có một ngày làm việc vui vẻ!<br>&#160;<br>******************************************************************************************</div>",
                    ContentParameters = @"Title;#WorkflowContent;#Author;#""vào đây""(link);#""here""(link)",
                    CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"}
                };
                await context.MailTemplates.AddRangeAsync(mailTemplates);
                await context.SaveChangesAsync();

                var propertyDefaultValueTypes = new PropertyDefaultValueType[]
                {
                    new PropertyDefaultValueType{Name="Value", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new PropertyDefaultValueType{Name="Variable", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new PropertyDefaultValueType{Name="UserProfile", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new PropertyDefaultValueType{Name="DepartmentProfile", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                };
                await context.PropertyDefaultValueTypes.AddRangeAsync(propertyDefaultValueTypes);
                await context.SaveChangesAsync();

                var propertyDefaultVariables = new PropertyDefaultVariable[]
                {
                    new PropertyDefaultVariable{Name="@CurrentUser", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserCode", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserPosition", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserEmail", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserLevel", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserPhone", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserDepartment", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@GetNow", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},
                    new PropertyDefaultVariable{Name="@GetToday", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="Variable")).Id},

                    new PropertyDefaultVariable{Name="@CurrentUser", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserCode", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserPosition", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserEmail", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserLevel", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserPhone", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},
                    new PropertyDefaultVariable{Name="@CurrentUserDepartment", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="UserProfile")).Id},

                    new PropertyDefaultVariable{Name="@CurrentUserDepartment", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin", DefaultTypeId = (await context.PropertyDefaultValueTypes.FirstOrDefaultAsync(p=>p.Name=="DepartmentProfile")).Id},
                };
                await context.PropertyDefaultVariables.AddRangeAsync(propertyDefaultVariables);
                await context.SaveChangesAsync();

                var assignmentRules = new AssignmentRule[]
                {
                    new AssignmentRule{Name="Initiator", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Line Manager (Initiator)", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Department Manager (Initiator)", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Department Manager (user completed the previous step)", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Assign users from previous step", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Manual assignment", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Choose user", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Department managers", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new AssignmentRule{Name="Send registration", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                };
                await context.AssignmentRules.AddRangeAsync(assignmentRules);
                await context.SaveChangesAsync();

                var departmentsDeptLevel1 = new Department[]
                {
                    new Department{Code = "VNTT", Title = "Công Ty Cổ Phần Công Nghệ & Truyền Thông Việt Nam", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "Becamex", Title = "Becamex", CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"}
                };
                await context.Departments.AddRangeAsync(departmentsDeptLevel1);
                await context.SaveChangesAsync();

                var departmentsDeptLevel2 = new Department[]
                {
                    new Department{Code = "BDH", Title = "Ban Điều Hành", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="VNTT")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "HOIDONGQUANTRI", Title = "HỘI ĐỒNG QUẢN TRỊ", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="VNTT")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"}
                    };
                await context.Departments.AddRangeAsync(departmentsDeptLevel2);
                await context.SaveChangesAsync();

                var departmentsDeptLevel3 = new Department[]
                {
                    new Department{Code = "MEP", Title = "Phòng Cơ - Điện (M.E.P)", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PDA", Title = "Phòng Dự Án", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PHCNS", Title = "Phòng Hành chính Nhân sự", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PKHMH", Title = "Phòng Kế Hoạch - Mua Hàng", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PKD", Title = "Phòng Kinh Doanh", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PKTVT", Title = "Phòng Kỹ Thuật Viễn Thông", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PQLTC", Title = "Phòng Quản Lý Tài Chính", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PQTDC", Title = "Phòng Quản Trị DC", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PVHDC", Title = "Phòng Vận Hành DC", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "TTL", Title = "Tổ trợ lý", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "R&D", Title = "Trung tâm nghiên cứu & phát triển (R&D)", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="BDH")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"}
                };
                await context.Departments.AddRangeAsync(departmentsDeptLevel3);
                await context.SaveChangesAsync();

                var departmentsDeptLevel4 = new Department[]
                {
                    new Department{Code = "PKDTTGD", Title = "Phòng Kinh Doanh - TT Giao dịch", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="PKD")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PKDKDTT", Title = "Phòng Kinh Doanh - KD Thường trực", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="PKD")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PKDTTGPDN", Title = "Phòng Kinh Doanh - Trung tâm GPDN", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="PKD")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                    new Department{Code = "PKDTTDVKH", Title = "Phòng Kinh Doanh - TT Dịch vụ khách hàng", ParentId = (await context.Departments.FirstOrDefaultAsync(p=>p.Code=="PKD")).Id, CreatedTime= DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin"},
                };
                await context.Departments.AddRangeAsync(departmentsDeptLevel4);
                await context.SaveChangesAsync();

                var workflowCategories = new WorkflowCategory[]
                {
                    new WorkflowCategory{ Title = "Chung", Order = 1, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "KHMH_Phòng Kế hoạch mua hàng", Order = 2, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "QLTC_Phòng Quản lý tài chính", Order = 3, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "HCNS_Phòng Hành chính nhân sự", Order=  4, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "M.E.P_Phòng MEP", Order = 5, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "HC-QT11_QT khắc phục sự không phù hợp", Order = 6, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "HC-QT12_QT đánh giá nội bộ", Order = 7, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "HC-QT03_QT tuyển dụng thử việc", Order = 8, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "HC-QT02_QT đào tạo - bồi dưỡng", Order = 9, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "HC-QT15", Order = 10, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                    new WorkflowCategory{ Title = "QT_NTTeA", Order = 11, CreatedTime = DateTime.UtcNow, IsDeleted = false, CreatedBy = "Admin" },
                };
                await context.WorkflowCategories.AddRangeAsync(workflowCategories);
                await context.SaveChangesAsync();

                await Task.CompletedTask;
            }
        }
    }
}
