﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/workflowCategories")]//required for default versioning
    [Route("api/v{version:apiVersion}/workflowCategories")]
    [ApiController]
    public class WorkflowCategoryController : Controller
    {
        private IWorkflowCategoryService _workflowCategoryService;

        public WorkflowCategoryController(IWorkflowCategoryService workflowCategoryService)
        {
            _workflowCategoryService = workflowCategoryService;
        }

        // GET: api/workflowCategories
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _workflowCategoryService.GetWorkflowCategories(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/workflowCategories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _workflowCategoryService.GetWorkflowCategory(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/workflowCategories
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WorkflowCategoryResource WorkflowCategoryResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowCategoryService.CreateWorkflowCategory(WorkflowCategoryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/workflowCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] WorkflowCategoryResource WorkflowCategoryResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowCategoryService.UpdateWorkflowCategory(id, WorkflowCategoryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/workflowCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _workflowCategoryService.DeleteWorkflowCategory(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
