﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/workflowSteps")]//required for default versioning
    [Route("api/v{version:apiVersion}/workflowSteps")]
    [ApiController]
    public class WorkflowStepController : Controller
    {
        private IWorkflowStepService _workflowStepService;

        public WorkflowStepController(IWorkflowStepService workflowStepService)
        {
            _workflowStepService = workflowStepService;
        }

        // GET: api/workflowSteps
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _workflowStepService.GetWorkflowSteps(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/workflowSteps/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _workflowStepService.GetWorkflowStep(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/workflowSteps
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WorkflowStepResource workflowStepResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowStepService.CreateWorkflowStep(workflowStepResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/workflowSteps/list
        [HttpPost]
        [Route("list")]
        public async Task<IActionResult> PostList([FromBody] List<WorkflowStepResource> workflowStepResources)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowStepService.CreateWorkflowSteps(workflowStepResources);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/workflowSteps/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] WorkflowStepResource WorkflowStepResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowStepService.UpdateWorkflowStep(id, WorkflowStepResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/workflowSteps/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _workflowStepService.DeleteWorkflowStep(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
