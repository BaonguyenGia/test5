using Microsoft.AspNetCore.Mvc;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/healthcheck")]//required for default versioning
    [Route("api/v{version:apiVersion}/healthcheck")]
    [ApiController]
    public class HealthCheckController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Ok");
        }
    }
}