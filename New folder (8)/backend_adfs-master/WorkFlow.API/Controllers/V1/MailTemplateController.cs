﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/mailTemplates")]//required for default versioning
    [Route("api/v{version:apiVersion}/mailTemplates")]
    [ApiController]
    public class MailTemplateController : Controller
    {
        private IMailTemplateService _mailTemplateService;

        public MailTemplateController(IMailTemplateService mailTemplateService)
        {
            _mailTemplateService = mailTemplateService;
        }

        // GET: api/mailTemplates
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _mailTemplateService.GetMailTemplates(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/mailTemplates/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _mailTemplateService.GetMailTemplate(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/mailTemplates
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MailTemplateResource mailTemplateResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _mailTemplateService.CreateMailTemplate(mailTemplateResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/mailTemplates/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] MailTemplateResource mailTemplateResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _mailTemplateService.UpdateMailTemplate(id, mailTemplateResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/mailTemplates/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _mailTemplateService.DeleteMailTemplate(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
