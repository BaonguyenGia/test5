﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/workingTimes")]//required for default versioning
    [Route("api/v{version:apiVersion}/workingTimes")]
    [ApiController]
    public class WorkingTimeController : Controller
    {
        private IWorkingTimeService _workingTimeService;

        public WorkingTimeController(IWorkingTimeService workingTimeService)
        {
            _workingTimeService = workingTimeService;
        }

        // GET: api/workingTimes
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _workingTimeService.GetWorkingTimes(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/workingTimes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _workingTimeService.GetWorkingTime(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/workingTimes
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WorkingTimeResource WorkingTimeResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workingTimeService.CreateWorkingTime(WorkingTimeResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/workingTimes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] WorkingTimeResource WorkingTimeResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workingTimeService.UpdateWorkingTime(id, WorkingTimeResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/workingTimes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _workingTimeService.DeleteWorkingTime(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
