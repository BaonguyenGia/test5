﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/documentTypes")]//required for default versioning
    [Route("api/v{version:apiVersion}/documentTypes")]
    [ApiController]
    public class DocumentTypeController : Controller
    {
        private IDocumentTypeService _documentTypeService;

        public DocumentTypeController(IDocumentTypeService documentTypeService)
        {
            _documentTypeService = documentTypeService;
        }

        // GET: api/documentTypes
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _documentTypeService.GetDocumentTypes(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/documentTypes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _documentTypeService.GetDocumentType(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/documentTypes
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DocumentTypeResource documentTypeResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _documentTypeService.CreateDocumentType(documentTypeResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/documentTypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] DocumentTypeResource documentTypeResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _documentTypeService.UpdateDocumentType(id, documentTypeResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/documentTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _documentTypeService.DeleteDocumentType(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/documentTypes/importFromList
        [HttpPost("ImportFromList")]
        public async Task<IActionResult> ImportFromList([FromBody] List<string> documentTypeTitles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _documentTypeService.ImportFromList(documentTypeTitles);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
