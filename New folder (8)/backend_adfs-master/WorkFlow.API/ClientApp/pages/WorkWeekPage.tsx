import "@Styles/main.scss";
import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { IWorkWeekModel } from "@Models/IWorkWeekModel";
import * as workWeekStore from "@Store/workWeekStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import PositionEditor from "@Components/position/PositionEditor";
import WorkWeekEditor from "@Components/workweek/WorkWeekEditor";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import {
  Modal,
  Button,
  Container,
  Row,
  Card,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import Result from "@Core/Result";
import {
  Pencil,
  Trash,
  Save,
  XCircle,
  Trash2,
  PlusSquare,
  X,
  ArrowClockwise,
  ThreeDots,
} from "react-bootstrap-icons";

type Props = typeof workWeekStore.actionCreators &
  workWeekStore.IWorkWeekStoreState &
  RouteComponentProps<{}>;

interface IState {
  searchTerm: string;
  currentPageNum: number;
  limitPerPage: number;
  isAddModalOpen: boolean;
  isUpdateModalOpen: boolean;
  isDeleteModalOpen: boolean;
  modelForEdit?: IWorkWeekModel;
}

class WorkWeekPage extends React.Component<Props, IState> {
  private paginator: Paginator;

  private debouncedSearch: (term: string) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchTerm: "",
      currentPageNum: 1,
      limitPerPage: 8,
      modelForEdit: null,
      isAddModalOpen: false,
      isDeleteModalOpen: false,
      isUpdateModalOpen: false,
    };

    // "AwesomeDebouncePromise" makes a delay between
    // the end of input term and search request.
    this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
      props.search(term);
    }, 500);

    
    wait(async () => {
      // Lets tell Node.js to wait for the request completion.
      // It's necessary when you want to see the fethched data
      // in your prerendered HTML code (by SSR).
      await this.props.search();
    }, "positionPageTask");
  }

  private refresh = () => {
    this.setState({ currentPageNum: 1 });
    wait(async () => {
      await this.props.search();
    }, "WorkflowCategoryPageTask");
  };

  private toggleAddPositionModal = () => {
    this.setState((prev) => ({
      isAddModalOpen: !prev.isAddModalOpen,
    }));
  };

  private toggleUpdatePositionModal = (modelForEdit?: IWorkWeekModel) => {
    this.setState((prev) => ({
      modelForEdit,
      isUpdateModalOpen: !prev.isUpdateModalOpen,
    }));
  };

  private toggleDeletePositionModal = (modelForEdit?: IWorkWeekModel) => {
    this.setState((prev) => ({
      modelForEdit,
      isDeleteModalOpen: !prev.isDeleteModalOpen,
    }));
  };

  private addPosition = async (data: IWorkWeekModel) => {
    const { add } = this.props;
    var result = (await add(data)) as any as Result<IWorkWeekModel>;

    if (!result.hasErrors) {
      this.paginator.setLastPage();
      this.toggleAddPositionModal();
    }
  };

  private updatePosition = async (data: IWorkWeekModel) => {
    const { update } = this.props;
    var result = await getPromiseFromActionCreator(update(data));

    if (!result.hasErrors) {
      this.toggleUpdatePositionModal();
    }
  };

  private deletePosition = async () => {
    const { remove } = this.props;
    const { modelForEdit } = this.state;

    await remove(modelForEdit.id);
    this.toggleDeletePositionModal();
  };

  private renderRows = (data: IWorkWeekModel[]) => {
    const { currentPageNum, limitPerPage } = this.state;

    return paginate(data, currentPageNum, limitPerPage).map((item) => (
      <tr key={item.id}>
        <td>{item.title}</td>
        <td>{item.isFullTime === true ? "Có" : "Không" }</td>
        <td>
          <button
            className="btn btn-info"
            onClick={(x) => this.toggleUpdatePositionModal(item)}
          >
            <Pencil />
          </button>
          &nbsp;
          <button
            className="btn btn-danger"
            onClick={(x) => this.toggleDeletePositionModal(item)}
          >
            <Trash />
          </button>
        </td>
      </tr>
    ));
  };

  private onClearSearchInput = () => {
    var val = "";
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    var val = e.currentTarget.value;
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  render() {
    const { items, isFetching } = this.props;
    const {
      isAddModalOpen,
      isUpdateModalOpen,
      modelForEdit,
      isDeleteModalOpen,
      limitPerPage,
      currentPageNum,
      searchTerm,
    } = this.state;

    return (
      <Container>
        <Helmet>
          <title>Cấu hình tuần làm việc</title>
        </Helmet>

        <Card body className="mt-4 mb-4">
          <Row>
            <div className="col-8 col-sm-9 col-md-9 col-lg-8">
              <InputGroup className="mb-3">
                <FormControl
                  placeholder={"Tìm kiếm theo tiêu đề..."}
                  defaultValue={""}
                  value={searchTerm}
                  onChange={this.onChangeSearchInput}
                />
                {searchTerm.trim().length > 0 && (
                  <Button variant="secondary" onClick={this.onClearSearchInput}>
                    <X />
                  </Button>
                )}
              </InputGroup>
            </div>
            <div className="col-4 col-sm-3 col-md-3 col-lg-4">
              <button
                className="btn btn-success"
                onClick={(x) => this.toggleAddPositionModal()}
              >
                Thêm mới
              </button>
              <Button
                className="button-reload"
                disabled={isFetching}
                onClick={(x) => this.refresh()}
              >
                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
              </Button>
            </div>
          </Row>
        </Card>

        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Fulltime</th>
              
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderRows(items)}</tbody>
        </table>

        {/* Add modal */}
        <Modal
          show={isAddModalOpen}
          onHide={() => this.toggleAddPositionModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>Thêm mới</Modal.Title>
          </Modal.Header>

          <WorkWeekEditor
            data={{} as IWorkWeekModel}
            onSubmit={this.addPosition}
            isEditted={false}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleAddPositionModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="success" onClick={(x) => handleSubmit()}>
                    <PlusSquare /> Thêm
                  </Button>
                </Modal.Footer>
              </>
            )}
          </WorkWeekEditor>
        </Modal>

        {/* Update modal */}
        <Modal
          show={isUpdateModalOpen}
          onHide={() => this.toggleUpdatePositionModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Chỉnh sửa cấu hình: {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>

          <WorkWeekEditor
            data={modelForEdit}
            onSubmit={this.updatePosition}
            isEditted={true}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleUpdatePositionModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="primary" onClick={(x) => handleSubmit()}>
                    <Save /> Lưu
                  </Button>
                </Modal.Footer>
              </>
            )}
          </WorkWeekEditor>
        </Modal>

        {/* Delete modal */}
        <Modal
          show={isDeleteModalOpen}
          onHide={() => this.toggleDeletePositionModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Xóa tiêu đề: {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Bạn có chắc chắn muốn xóa?</p>
       </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => this.toggleDeletePositionModal()}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button variant="danger" onClick={(x) => this.deletePosition()}>
              <Trash2 /> Xóa
            </Button>
          </Modal.Footer>
        </Modal>

        <Paginator
          ref={(x) => (this.paginator = x)}
          totalResults={items.length}
          limitPerPage={limitPerPage}
          currentPage={currentPageNum}
          onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
        />
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  WorkWeekPage,
  (state) => state.workweek, // Selects which state properties are merged into the component's props.
  workWeekStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
