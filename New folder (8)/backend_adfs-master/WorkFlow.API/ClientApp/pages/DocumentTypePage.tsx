﻿import "@Styles/main.scss";
import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { IDocumentTypeModel } from "@Models/IDocumentTypeModel";
import * as documentTypeStore from "@Store/documentTypeStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import DocumentTypeEditor from "@Components/documentType/DocumentTypeEditor";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import {
    Modal,
    Button,
    Container,
    Row,
    Card,
    InputGroup,
    FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import Result from "@Core/Result";
import {
    Save,
    XCircle,
    Trash,
    PlusSquare,
    X,
    ArrowClockwise,
    ThreeDots,
    Plus,
} from "react-bootstrap-icons";

type Props = typeof documentTypeStore.actionCreators &
    documentTypeStore.IDocumentTypeStoreState &
    RouteComponentProps<{}>;

interface IState {
    searchTerm: string;
    currentPageNum: number;
    limitPerPage: number;
    isAddModalOpen: boolean;
    isUpdateModalOpen: boolean;
    isDeleteModalOpen: boolean;
    modelForEdit?: IDocumentTypeModel;
}

class DocumentTypePage extends React.Component<Props, IState> {
    private paginator: Paginator;

    private debouncedSearch: (term: string) => void;

    constructor(props: Props) {
        super(props);

        this.state = {
            searchTerm: "",
            currentPageNum: 1,
            limitPerPage: 10,
            modelForEdit: null,
            isAddModalOpen: false,
            isDeleteModalOpen: false,
            isUpdateModalOpen: false,
        };

        // "AwesomeDebouncePromise" makes a delay between
        // the end of input term and search request.
        this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
            props.search(term);
        }, 500);


        this.props.search();
    }

    private refresh = () => {
        this.setState({ currentPageNum: 1 });
        wait(async () => {
            await this.props.search();
        }, "WorkflowCategoryPageTask");
    };

    private toggleAddDocumentTypeModal = () => {
        this.setState((prev) => ({
            isAddModalOpen: !prev.isAddModalOpen,
        }));
    };

    private toggleModelForEdit = (modelForEdit?: IDocumentTypeModel) => {
        this.setState((prev) => ({
            modelForEdit,
        }));
    };

    private toggleUpdateDocumentTypeModal = (modelForEdit?: IDocumentTypeModel) => {
        this.setState((prev) => ({
            modelForEdit,
            isUpdateModalOpen: !prev.isUpdateModalOpen,
        }));
    };

    private toggleDeleteDocumentTypeModal = () => {
        const { modelForEdit } = this.state;
        if (modelForEdit == null) {
            return;
        }
        this.setState((prev) => ({
            modelForEdit: prev.isDeleteModalOpen === true ? null : prev.modelForEdit,
            isDeleteModalOpen: !prev.isDeleteModalOpen,
        }));
    };

    private addDocumentType = async (data: IDocumentTypeModel) => {
        const { add } = this.props;
        var result = (await add(data)) as any as Result<IDocumentTypeModel>;

        if (!result.hasErrors) {
            this.paginator.setLastPage();
            this.toggleAddDocumentTypeModal();
        }
    };

    private updateDocumentType = async (data: IDocumentTypeModel) => {
        const { update } = this.props;
        var result = await getPromiseFromActionCreator(update(data));

        if (!result.hasErrors) {
            this.toggleUpdateDocumentTypeModal();
        }
    };

    private deleteDocumentType = async () => {
        const { remove } = this.props;
        const { modelForEdit } = this.state;

        await remove(modelForEdit.id);
        this.toggleDeleteDocumentTypeModal();
    };

    private renderRows = (data: IDocumentTypeModel[]) => {
        const { currentPageNum, limitPerPage, modelForEdit } = this.state;

        return paginate(data, currentPageNum, limitPerPage).map((item) => (
            <tr
                key={item.id}
                className={`subrow
          ${modelForEdit && item.id === modelForEdit.id ? " tr-selected" : ""}`}
                onClick={() => this.toggleModelForEdit(item)}
            >
                <td>
                    <a
                        className="subtitle"
                        href="javascript:void(0)"
                        onClick={(x) => this.toggleUpdateDocumentTypeModal(item)}
                    >
                        {item.title}
                    </a>
                </td>
                <td>{item.description}</td>
                <td>
                    {item.lastModified
                        ? formatTimeInEntity(item.lastModified)
                        : formatTimeInEntity(item.createdTime)}
                </td>
            </tr>
        ));
    };

    private onClearSearchInput = () => {
        var val = "";
        this.debouncedSearch(val);
        this.paginator.setFirstPage();
        this.setState({ searchTerm: val });
    };

    private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        var val = e.currentTarget.value;
        this.debouncedSearch(val);
        this.paginator.setFirstPage();
        this.setState({ searchTerm: val });
    };

    render() {
        const { items, isFetching } = this.props;
        const {
            isAddModalOpen,
            isUpdateModalOpen,
            modelForEdit,
            isDeleteModalOpen,
            limitPerPage,
            currentPageNum,
            searchTerm,
        } = this.state;

        return (
            <Container>
                <Helmet>
                    <title>Chức vụ - All Items</title>
                </Helmet>

                <Card body className="mt-4 mb-4">
                    <Row>
                        <div className="col-8 col-sm-9 col-md-9 col-lg-8">
                            <InputGroup className="mb-3">
                                <FormControl
                                    placeholder={"Tìm kiếm theo tiêu đề..."}
                                    defaultValue={""}
                                    value={searchTerm}
                                    onChange={this.onChangeSearchInput}
                                />
                                {searchTerm.trim().length > 0 && (
                                    <Button variant="secondary" onClick={this.onClearSearchInput}>
                                        <X />
                                    </Button>
                                )}
                            </InputGroup>
                        </div>
                        <div className="col-4 col-sm-3 col-md-3 col-lg-4">
                            <Button
                                className="button-reload"
                                disabled={isFetching}
                                onClick={(x) => this.refresh()}
                            >
                                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
                            </Button>
                        </div>
                    </Row>
                </Card>
                <div className="option-panel">
                    <div onClick={() => this.toggleAddDocumentTypeModal()} className="add">
                        <Plus size={30} color="green" /> Tạo mới
          </div>
                    <div onClick={() => this.toggleDeleteDocumentTypeModal()}>
                        <Trash size={20} color="red" /> Xóa
          </div>
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Tiêu đề</th>
                            <th>Mô tả</th>
                            <th>Đã sửa đổi</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderRows(items)}</tbody>
                </table>

                {/* Add modal */}
                <Modal
                    show={isAddModalOpen}
                    onHide={() => this.toggleAddDocumentTypeModal()}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Thêm mới</Modal.Title>
                    </Modal.Header>

                    <DocumentTypeEditor
                        data={{} as IDocumentTypeModel}
                        onSubmit={this.addDocumentType}
                        isEditted={false}
                    >
                        {(renderEditor, handleSubmit) => (
                            <>
                                <Modal.Body>{renderEditor()}</Modal.Body>
                                <Modal.Footer>
                                    <Button
                                        variant="secondary"
                                        onClick={(x) => this.toggleAddDocumentTypeModal()}
                                    >
                                        <XCircle /> Hủy bỏ
                  </Button>
                                    <Button variant="success" onClick={(x) => handleSubmit()}>
                                        <PlusSquare /> Thêm
                  </Button>
                                </Modal.Footer>
                            </>
                        )}
                    </DocumentTypeEditor>
                </Modal>

                {/* Update modal */}
                <Modal
                    show={isUpdateModalOpen}
                    onHide={() => this.toggleUpdateDocumentTypeModal()}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Chỉnh sửa chức vụ: {modelForEdit ? `${modelForEdit.title}` : null}
                        </Modal.Title>
                    </Modal.Header>

                    <DocumentTypeEditor
                        data={modelForEdit}
                        onSubmit={this.updateDocumentType}
                        isEditted={true}
                    >
                        {(renderEditor, handleSubmit) => (
                            <>
                                <Modal.Body>{renderEditor()}</Modal.Body>
                                <Modal.Footer>
                                    <Button
                                        variant="secondary"
                                        onClick={(x) => this.toggleUpdateDocumentTypeModal()}
                                    >
                                        <XCircle /> Hủy bỏ
                  </Button>
                                    <Button variant="primary" onClick={(x) => handleSubmit()}>
                                        <Save /> Lưu
                  </Button>
                                </Modal.Footer>
                            </>
                        )}
                    </DocumentTypeEditor>
                </Modal>

                {/* Delete modal */}
                <Modal
                    show={isDeleteModalOpen}
                    onHide={() => this.toggleDeleteDocumentTypeModal()}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Xóa tiêu đề: {modelForEdit ? `${modelForEdit.title}` : null}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Bạn có chắc chắn muốn xóa chức vụ này không?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            variant="secondary"
                            onClick={(x) => this.toggleDeleteDocumentTypeModal()}
                        >
                            <XCircle /> Hủy bỏ
            </Button>
                        <Button variant="danger" onClick={(x) => this.deleteDocumentType()}>
                            <Trash /> Xóa
            </Button>
                    </Modal.Footer>
                </Modal>

                <Paginator
                    ref={(x) => (this.paginator = x)}
                    totalResults={items.length}
                    limitPerPage={limitPerPage}
                    currentPage={currentPageNum}
                    onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
                />
            </Container>
        );
    }
}

// Connect component with Redux store.
var connectedComponent = withStore(
    DocumentTypePage,
    (state) => state.documentType, // Selects which state properties are merged into the component's props.
    documentTypeStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
