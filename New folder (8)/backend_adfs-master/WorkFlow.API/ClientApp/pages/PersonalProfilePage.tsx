import "@Styles/main.scss";
import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import * as personalProfileStore from "@Store/personalProfileStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import PersonalProfileEditor from "@Components/personalProfile/PersonalProfileEditor";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import {
  Modal,
  Button,
  Container,
  Row,
  Card,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import Result from "@Core/Result";
import {
  Save,
  XCircle,
  Trash,
  PlusSquare,
  X,
  ArrowClockwise,
  ThreeDots,
  Plus,
} from "react-bootstrap-icons";

type Props = typeof personalProfileStore.actionCreators &
  personalProfileStore.IPersonalProfileStoreState &
  RouteComponentProps<{}>;

interface IState {
  searchTerm: string;
  currentPageNum: number;
  limitPerPage: number;
  isAddModalOpen: boolean;
  isUpdateModalOpen: boolean;
  isDeleteModalOpen: boolean;
  modelForEdit?: IPersonalProfileModel;
}

class PersonalProfilePage extends React.Component<Props, IState> {
  private paginator: Paginator;

  private debouncedSearch: (term: string) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchTerm: "",
      currentPageNum: 1,
      limitPerPage: 14,
      modelForEdit: null,
      isAddModalOpen: false,
      isDeleteModalOpen: false,
      isUpdateModalOpen: false,
    };

    // "AwesomeDebouncePromise" makes a delay between
    // the end of input term and search request.
    this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
      props.search(term);
    }, 500);

    // wait(async () => {
    //   // Lets tell Node.js to wait for the request completion.
    //   // It's necessary when you want to see the fethched data
    //   // in your prerendered HTML code (by SSR).

    // }, "personalProfilePageTask");
    this.props.search();
    this.props.searchSubData();
  }

  private refresh = () => {
    this.setState({ currentPageNum: 1 });
    wait(async () => {
      await this.props.search();
    }, "WorkflowCategoryPageTask");
  };

  private toggleAddPersonalProfileModal = () => {
    this.setState((prev) => ({
      isAddModalOpen: !prev.isAddModalOpen,
    }));
  };

  private toggleModelForEdit = (modelForEdit?: IPersonalProfileModel) => {
    this.setState((prev) => ({
      modelForEdit,
    }));
  };

  private toggleUpdatePersonalProfileModal = (
    modelForEdit?: IPersonalProfileModel
  ) => {
    this.setState((prev) => ({
      modelForEdit,
      isUpdateModalOpen: !prev.isUpdateModalOpen,
    }));
  };

  private toggleDeletePersonalProfileModal = () => {
    const { modelForEdit } = this.state;
    if (modelForEdit == null) {
      return;
    }
    this.setState((prev) => ({
      modelForEdit: prev.isDeleteModalOpen === true ? null : prev.modelForEdit,
      isDeleteModalOpen: !prev.isDeleteModalOpen,
    }));
  };

  private addPersonalProfile = async (data: IPersonalProfileModel) => {
    const { add } = this.props;
    var result = (await add(data)) as any as Result<IPersonalProfileModel>;

    if (!result.hasErrors) {
      this.paginator.setLastPage();
      this.toggleAddPersonalProfileModal();
    }
  };

  private updatePersonalProfile = async (data: IPersonalProfileModel) => {
    const { update } = this.props;
    var result = await getPromiseFromActionCreator(update(data));

    if (!result.hasErrors) {
      this.toggleUpdatePersonalProfileModal();
    }
  };

  private deletePersonalProfile = async () => {
    const { remove } = this.props;
    const { modelForEdit } = this.state;

    await remove(modelForEdit.id);
    this.toggleDeletePersonalProfileModal();
  };

  private renderRows = (data: IPersonalProfileModel[]) => {
    const { currentPageNum, limitPerPage, modelForEdit } = this.state;

    return paginate(data, currentPageNum, limitPerPage).map((item) => (
      <tr
        key={item.id}
        className={`subrow
          ${modelForEdit && item.id === modelForEdit.id ? " tr-selected" : ""}`}
        onClick={() => this.toggleModelForEdit(item)}
      >
        <td>
          <a
            className="subtitle"
            href="javascript:void(0)"
            onClick={(x) => this.toggleUpdatePersonalProfileModal(item)}
          >
            {item.fullName}
          </a>
        </td>
        <td>{item.name}</td>
        <td>{item.email}</td>
        <td>{item.positionTitle}</td>
        <td>{item.departmentTitle}</td>
      </tr>
    ));
  };

  private onClearSearchInput = () => {
    var val = "";
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    var val = e.currentTarget.value;
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  render() {
    const { items, isFetching, positionOptions, departmentOptions } =
      this.props;
    const {
      isAddModalOpen,
      isUpdateModalOpen,
      modelForEdit,
      isDeleteModalOpen,
      limitPerPage,
      currentPageNum,
      searchTerm,
    } = this.state;

    return (
      <Container>
        <Helmet>
          <title>nhân viên - All Items</title>
        </Helmet>

        <Card body className="mt-4 mb-4">
          <Row>
            <div className="col-8 col-sm-9 col-md-9 col-lg-8">
              <InputGroup className="mb-3">
                <FormControl
                  placeholder={"Tìm kiếm theo tiêu đề..."}
                  defaultValue={""}
                  value={searchTerm}
                  onChange={this.onChangeSearchInput}
                />
                {searchTerm.trim().length > 0 && (
                  <Button variant="secondary" onClick={this.onClearSearchInput}>
                    <X />
                  </Button>
                )}
              </InputGroup>
            </div>
            <div className="col-4 col-sm-3 col-md-3 col-lg-4">
              <Button
                className="button-reload"
                disabled={isFetching}
                onClick={(x) => this.refresh()}
              >
                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
              </Button>
            </div>
          </Row>
        </Card>
        <div className="option-panel">
          <div
            onClick={() => this.toggleAddPersonalProfileModal()}
            className="add"
          >
            <Plus size={30} color="green" /> Tạo mới
          </div>
          <div onClick={() => this.toggleDeletePersonalProfileModal()}>
            <Trash size={20} color="red" /> Xóa
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th>Họ tên</th>
              <th>Tên đăng nhập</th>
              <th>Email</th>
              <th>Chức vụ</th>
              <th>Đơn vị</th>
            </tr>
          </thead>
          <tbody>{this.renderRows(items)}</tbody>
        </table>

        {/* Add modal */}
        <Modal
          show={isAddModalOpen}
          onHide={() => this.toggleAddPersonalProfileModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>Thêm mới</Modal.Title>
          </Modal.Header>

          <PersonalProfileEditor
            data={{} as IPersonalProfileModel}
            onSubmit={this.addPersonalProfile}
            isEditted={false}
            positionOptions={positionOptions}
            departmentOptions={departmentOptions}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleAddPersonalProfileModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="success" onClick={(x) => handleSubmit()}>
                    <PlusSquare /> Thêm
                  </Button>
                </Modal.Footer>
              </>
            )}
          </PersonalProfileEditor>
        </Modal>

        {/* Update modal */}
        <Modal
          show={isUpdateModalOpen}
          onHide={() => this.toggleUpdatePersonalProfileModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Chỉnh sửa nhân viên:{" "}
              {modelForEdit ? `${modelForEdit.name}` : null}
            </Modal.Title>
          </Modal.Header>

          <PersonalProfileEditor
            data={modelForEdit}
            onSubmit={this.updatePersonalProfile}
            isEditted={true}
            positionOptions={positionOptions}
            departmentOptions={departmentOptions}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleUpdatePersonalProfileModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="primary" onClick={(x) => handleSubmit()}>
                    <Save /> Lưu
                  </Button>
                </Modal.Footer>
              </>
            )}
          </PersonalProfileEditor>
        </Modal>

        {/* Delete modal */}
        <Modal
          show={isDeleteModalOpen}
          onHide={() => this.toggleDeletePersonalProfileModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Xóa nhân viên: {modelForEdit ? `${modelForEdit.name}` : null}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Bạn có chắc chắn muốn xóa nhân viên này không?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => this.toggleDeletePersonalProfileModal()}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button
              variant="danger"
              onClick={(x) => this.deletePersonalProfile()}
            >
              <Trash /> Xóa
            </Button>
          </Modal.Footer>
        </Modal>

        <Paginator
          ref={(x) => (this.paginator = x)}
          totalResults={items.length}
          limitPerPage={limitPerPage}
          currentPage={currentPageNum}
          onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
        />
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  PersonalProfilePage,
  (state) => state.personalProfile, // Selects which state properties are merged into the component's props.
  personalProfileStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
