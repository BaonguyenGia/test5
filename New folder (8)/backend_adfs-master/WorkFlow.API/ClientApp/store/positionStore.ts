import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IPositionModel } from "@Models/IPositionModel";
import PositionService from "@Services/PositionService";

// Declare an interface of the store's state.
export interface IPositionStoreState {
  isFetching: boolean;
  items: IPositionModel[];
}

// Create the slice.
const slice = createSlice({
  name: "position",
  initialState: {
    isFetching: false,
    items: [],
  } as IPositionStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IPositionModel[]>) => {
      state.items = action.payload;
    },
    addData: (state, action: PayloadAction<IPositionModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IPositionModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PositionService();

    const result = await service.search(title);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  add: (model: IPositionModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PositionService();

    const result = await service.add(model);

    if (!result.hasErrors) {
      model.id = result.value;
      dispatch(slice.actions.addData(model));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  update: (model: IPositionModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PositionService();

    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PositionService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
