import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import PersonalProfileService from "@Services/PersonalProfileService";
import PositionService from "@Services/PositionService";
import { IPositionModel } from "@Models/IPositionModel";
import DepartmentService from "@Services/DepartmentService";
import { IDepartmentModel } from "@Models/IDepartmentModel";

// Declare an interface of the store's state.
export interface IPersonalProfileStoreState {
  isFetching: boolean;
  items: IPersonalProfileModel[];
  positionOptions: IPositionModel[];
  departmentOptions: IDepartmentModel[];
}

// Create the slice.
const slice = createSlice({
  name: "personalProfile",
  initialState: {
    isFetching: false,
    items: [],
    positionOptions: [],
  } as IPersonalProfileStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IPersonalProfileModel[]>) => {
      state.items = action.payload;
    },
    setPositions: (state, action: PayloadAction<IPositionModel[]>) => {
      state.positionOptions = action.payload;
    },
    setDepartment: (state, action: PayloadAction<IDepartmentModel[]>) => {
      state.departmentOptions = action.payload;
    },
    addData: (state, action: PayloadAction<IPersonalProfileModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IPersonalProfileModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.search(title);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  searchSubData: () => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const positionService = new PositionService();
    const departmentnService = new DepartmentService();

    const serviceResult = await positionService.search();
    const departmentResult = await departmentnService.search();

    if (!serviceResult.hasErrors) {
      dispatch(slice.actions.setPositions(serviceResult.value.items));
    }

    if (!departmentResult.hasErrors) {
      dispatch(slice.actions.setDepartment(departmentResult.value.items));
    }

    dispatch(slice.actions.setFetching(false));
  },
  add: (model: IPersonalProfileModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.add(model);

    if (!result.hasErrors) {
      model.id = result.value;
      dispatch(slice.actions.addData(model));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  update: (model: IPersonalProfileModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
