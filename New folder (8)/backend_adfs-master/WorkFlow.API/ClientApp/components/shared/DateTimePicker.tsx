import React, { useState } from "react";
import DatePicker from "react-datepicker";

export interface IProps {
  date: Date | null;
  chooseDateTime: (date: Date) => void;
}

const DateTimePicker: React.FC<IProps> = (props: IProps) => {
  const [startDate, setStartDate] = useState(
    props.date ? props.date : new Date()
  );

  return (
    <DatePicker
      dateFormat="dd/MM/yyyy"
      selected={startDate}
      onChange={(date) => {
        setStartDate(date);
        props.chooseDateTime(date);
      }}
    />
  );
};

export default DateTimePicker;
