import * as React from "react";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import { Formik, Field } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import { removeTime } from "@Services/FormatDateTimeService";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";
import DateTimePicker from "@Components/shared/DateTimePicker";
import { Typeahead } from "react-bootstrap-typeahead";
import { Button, Modal } from "react-bootstrap";
import { IPositionModel } from "@Models/IPositionModel";
import { IDepartmentModel } from "@Models/IDepartmentModel";

export interface IProps {
  data: IPersonalProfileModel;
  isEditted: boolean | null;
  positionOptions: IPositionModel[];
  departmentOptions: IDepartmentModel[];
  onSubmit: (data: IPersonalProfileModel) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const PersonalProfileEditor: React.FC<IProps> = (props: IProps) => {
  const formValidator = React.useRef<FormValidator>(null);
  const [positionSelected, setPositionSelected] = React.useState<
    IPositionModel[]
  >([]);
  React.useEffect(() => {
    var position = props.positionOptions.find(
      (p) => p.id === props.data?.positionId ?? 0
    );
    console.log(position);
    if (position != undefined) {
      const defaultSelected = [position];
      setPositionSelected(defaultSelected);
    }
  }, []);
  const onSubmitForm = (values: IPersonalProfileModel) => {
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values);
  };

  // This function will be passed to children components as a parameter.
  // It's necessary to build custom markup with controls outside this component.
  const renderEditor = (
    values: IPersonalProfileModel,
    setFieldValue: (
      field: string,
      value: any,
      shouldValidate?: boolean
    ) => void,
    defaultPositionSelected: IPositionModel[]
  ) => {
    return (
      <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
        <div className="form-group">
          <Field
            type="email"
            name={nameof.full<IPersonalProfileModel>((x) => x.email)}
          >
            {({ field }) => (
              <>
                <label
                  className="control-label required"
                  htmlFor="personalProfile__email"
                >
                  Tài khoản
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="personalProfile__email"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền email tài khoản."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                  disabled={props.isEditted ? true : false}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.fullName)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__fullName"
                >
                  Họ tên
                </label>
                <input
                  className="form-control"
                  id="personalProfile__fullName"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.staffId)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__staffId"
                >
                  Mã nhân viên
                </label>
                <input
                  className="form-control"
                  id="personalProfile__staffId"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.birthDay)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__birthDay"
                >
                  Ngày sinh
                </label>
                <div>
                  <DateTimePicker
                    date={field.birthDay}
                    chooseDateTime={(date) => {
                      setFieldValue("birthDay", removeTime(date));
                    }}
                  />
                </div>
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.address)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__address"
                >
                  Địa chỉ
                </label>
                <textarea
                  className="form-control"
                  id="personalProfile__address"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_TEXTAREA}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.mobile)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__mobile"
                >
                  Điện thoại
                </label>
                <textarea
                  className="form-control"
                  id="personalProfile__mobile"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_TEXTAREA}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.dateOfHire)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__dateOfHire"
                >
                  Ngày vào làm
                </label>
                <div>
                  <DateTimePicker
                    date={field.dateOfHire}
                    chooseDateTime={(date) => {
                      setFieldValue("dateOfHire", removeTime(date));
                    }}
                  />
                </div>
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.positionId)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="personalProfile__positionId"
                >
                  Chức vụ
                </label>
                <div className="type-head">
                  <Typeahead
                    paginationOption={null}
                    clearButton
                    id="personalProfile__positionId"
                    options={props.positionOptions}
                    placeholder="Chọn chức vụ..."
                    labelKey="title"
                    onChange={(s) => {
                      setFieldValue(
                        nameof.full<IPersonalProfileModel>((x) => x.positionId),
                        s.length > 0 ? s[0].id : 0
                      );
                      setPositionSelected(s);
                    }}
                    selected={positionSelected}
                    defaultSelected={defaultPositionSelected}
                  ></Typeahead>
                </div>
              </>
            )}
          </Field>
        </div>
      </FormValidator>
    );
  };

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        console.log(values);
        // onSubmitForm(values);
      }}
    >
      {({ values, setFieldValue, handleSubmit }) => {
        // Let's say that the children element is a parametrizable function.
        // So we will pass other elements to this functional component as children
        // elements of this one:
        // <PersonEditor>
        // {(renderEditor, handleSubmit) => <>
        //     {renderEditor()}
        //     <button onClick={x => handleSubmit()}>Submit</button>
        // </>}
        // </PersonEditor>.
        return props.children(
          () => renderEditor(values, setFieldValue, positionSelected),
          handleSubmit
        );
      }}
    </Formik>
  );
};

export default PersonalProfileEditor;
