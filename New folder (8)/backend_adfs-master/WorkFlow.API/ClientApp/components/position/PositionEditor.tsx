import * as React from "react";
import { IPositionModel } from "@Models/IPositionModel";
import { Formik, Field } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";

export interface IProps {
  data: IPositionModel;
  isEditted: boolean | null;
  onSubmit: (data: IPositionModel) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const PositionEditor: React.FC<IProps> = (props: IProps) => {
  const formValidator = React.useRef<FormValidator>(null);

  const onSubmitForm = (values: IPositionModel) => {
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values);
  };

  // This function will be passed to children components as a parameter.
  // It's necessary to build custom markup with controls outside this component.
  const renderEditor = (values: IPositionModel, isEditted: boolean | null) => {
    return (
      <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
        <div className="form-group">
          <Field name={nameof.full<IPositionModel>((x) => x.title)}>
            {({ field }) => (
              <>
                <label
                  className="control-label required"
                  htmlFor="position__firstName"
                >
                  Tiêu đề
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="position__title"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền tiêu đề."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPositionModel>((x) => x.description)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="position__description"
                >
                  Mô tả
                </label>
                <textarea
                  className="form-control"
                  id="position__description"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_TEXTAREA}
                />
              </>
            )}
          </Field>
        </div>

        {isEditted && (
          <div>
            <div className="form-group-divide">
              <div className="form-group">
                <Field name={nameof.full<IPositionModel>((x) => x.createdBy)}>
                  {({ field }) => (
                    <>
                      <label
                        className="control-label"
                        htmlFor="position__createdBy"
                      >
                        Người tạo
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="position__createdBy"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={field.value || ""}
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>

              <div className="form-group">
                <Field name={nameof.full<IPositionModel>((x) => x.createdTime)}>
                  {({ field }) => (
                    <>
                      <label className="control-label" htmlFor="createdTime">
                        Vào lúc
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="position__createdTime"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={
                          field.value ? formatTimeInEntity(field.value) : ""
                        }
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>
            </div>

            <div className="form-group-divide">
              <div className="form-group">
                <Field name={nameof.full<IPositionModel>((x) => x.modifieddBy)}>
                  {({ field }) => (
                    <>
                      <label
                        className="control-label"
                        htmlFor="position__createdBy"
                      >
                        Người chỉnh sửa
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="position__modifieddBy"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={field.value || ""}
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>

              <div className="form-group">
                <Field
                  name={nameof.full<IPositionModel>((x) => x.lastModified)}
                >
                  {({ field }) => (
                    <>
                      <label className="control-label" htmlFor="createdTime">
                        Vào lúc
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="position__lastModified"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={
                          field.value ? formatTimeInEntity(field.value) : ""
                        }
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>
            </div>
          </div>
        )}
      </FormValidator>
    );
  };

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        onSubmitForm(values);
      }}
    >
      {({ values, handleSubmit }) => {
        // Let's say that the children element is a parametrizable function.
        // So we will pass other elements to this functional component as children
        // elements of this one:
        // <PersonEditor>
        // {(renderEditor, handleSubmit) => <>
        //     {renderEditor()}
        //     <button onClick={x => handleSubmit()}>Submit</button>
        // </>}
        // </PersonEditor>.
        return props.children(
          () => renderEditor(values, props.isEditted),
          handleSubmit
        );
      }}
    </Formik>
  );
};

export default PositionEditor;
