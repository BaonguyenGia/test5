import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IWorkflowCategoryModel } from "@Models/IWorkflowCategoryModel";
import { IQueryResult } from "@Models/IQueryResult";

export default class WorkflowCategoryService extends ServiceBase {
  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IWorkflowCategoryModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IWorkflowCategoryModel>>({
      url: `/api/workflowCategories`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }

  public async update(model: IWorkflowCategoryModel): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/workflowCategories/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/workflowCategories/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IWorkflowCategoryModel): Promise<Result<number>> {
    var result = await this.requestJson<number>({
      url: "/api/workflowCategories",
      method: "POST",
      data: model,
    });
    return result;
  }
}
