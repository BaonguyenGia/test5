import moment from "moment";
import "moment/locale/vi";

export const formatTimeInEntity = (date: string) => {
  return moment.utc(date).local().locale("vi").format("LLL");
};

export const removeTime = (date: Date) => {
  return moment.utc(date).local().locale("vi").format("L");
};
