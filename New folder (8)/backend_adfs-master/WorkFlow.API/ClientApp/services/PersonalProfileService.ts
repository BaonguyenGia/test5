import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import { IQueryResult } from "@Models/IQueryResult";

export default class PersonalProfileService extends ServiceBase {
  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IPersonalProfileModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IPersonalProfileModel>>({
      url: `/api/personalProfiles`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }

  public async update(model: IPersonalProfileModel): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/personalProfiles/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/personalProfiles/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IPersonalProfileModel): Promise<Result<number>> {
    var result = await this.requestJson<number>({
      url: "/api/personalProfiles",
      method: "POST",
      data: model,
    });
    return result;
  }
}
