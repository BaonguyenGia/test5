import { IEngineEntity } from "@Models/IEngineEntity";

export interface IPersonalProfileModel extends IEngineEntity {
  accountId: string;
  accountName: string;
  name: string;
  fullName: string;
  departmentId: number;
  departmentTitle: string;
  managerId: number | null;
  positionId: number | null;
  positionTitle: string;
  gender: boolean;
  birthDay: Date | null;
  address: string;
  staffId: string;
  dateOfHire: Date | null;
  mobile: string;
  email: string;
  imagePath: string;
  signatureUserName: string;
  signaturePassword: string;
  enableSignature: boolean;
  userStatus: number | null;
}
