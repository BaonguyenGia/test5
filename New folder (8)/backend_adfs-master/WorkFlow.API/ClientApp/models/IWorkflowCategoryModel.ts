import { IEngineEntity } from "@Models/IEngineEntity";

export interface IWorkflowCategoryModel extends IEngineEntity {
  title: string;
  order: number;
}
