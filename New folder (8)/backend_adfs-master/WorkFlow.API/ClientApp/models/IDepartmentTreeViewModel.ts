export interface IDepartmentTreeViewModel {
  id: number;
  parentId: number | null;
  label: string;
  items: IDepartmentTreeViewModel[];
}
