export interface IEngineEntity {
    id: number;
    createdBy: string;
    createdTime: string | null;
    lastModified: string | null;
    modifieddBy: string;
    isDeleted: boolean;
}